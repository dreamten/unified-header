$(document).ready(function () {
    // Remove touch devices tap delay
    $(function() {
        FastClick.attach(document.body);
    });

    // Detect window width
    var isMobile = false;
    function check_window_width(){
        var window_width = $(window).width();
        if(window_width < 768){
            $('body').addClass('mobile');
            isMobile = true;
        }
        else{
            $('body').removeClass('mobile');
            isMobile = false;
        }
    }
    check_window_width();
    var check = true;
    $(window).resize(function () {
        if(check){
            check = false;
            setTimeout(function(){ check_window_width(); check = true }, 100);
        }
    });

    // Ticketing window refresh button
    var refresh_button = $('.nav-window .refresh-button');
    refresh_button.click(function () {
        var refresh_box = $(this).closest('.nav-window').find('.refresh-box');
        var e = $(this);
        e.addClass('rotate-once');
        refresh_box.slideDown(function () {
            setTimeout(
                function () {
                    refresh_box.slideUp();
                    e.removeClass('rotate-once');
                }, 1000);
        });
    });

    // Detect IOS and Mobile Safari
    var iOS = /iPhone|iPod/.test(navigator.platform);
    var MobileSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);

    // Ticketing window comment input
    var comment_input = $('.nav-window .comment-input-container textarea');
    var ticket_content = $('.ticket-content');
    var container = $('.comment-input-container');
    var container_clone = container.clone().addClass('no-transition').appendTo('.dummy');
    var container_height_before = container_clone.outerHeight();
    ticket_content.css('padding-bottom', container_height_before);
    comment_input.focusin(function () {
        container = $(this).closest('.comment-input-container');
        container_clone = container.clone().addClass('no-transition open').appendTo('.dummy');
        var shown_tab = $('.ticket-tabs').find('.tab.shown');
        container.addClass('open');
        var container_height_after = container_clone.outerHeight();
        if(!isMobile){
            shown_tab.css('max-height', 375 - (container_height_after - container_height_before));
        }
        ticket_content.css('padding-bottom', container_height_after);
        if(iOS && MobileSafari){
            container.css('bottom', '120px');
        }

        $(document).click(function(e){
            if( $(e.target).closest(".comment-input-container").length > 0 ) {
                return false;
            }
            container.removeClass('open');
            container_clone.remove();
            if(!isMobile){
                shown_tab.css('max-height', 375);
            }
            ticket_content.css('padding-bottom', container_height_before);
            if(iOS && MobileSafari){
                container.css('bottom', '0');
            }
        });
    });
    comment_input.focusout(function () {
        if(iOS && MobileSafari){
            container.css('bottom', '0');
        }
    });

    // Ticketing window ticket tabs
    var ticket_tabs = $('.ticket-tabs');
    var tab_button = ticket_tabs.find('.tab-button');
    var tab = ticket_tabs.find('.tab');
    tab_button.click(function () {
        tab_button.removeClass('active');
        tab.removeClass('shown');
        $(this).addClass('active');
        if ($(this).hasClass('tab-comments-button')) {
            ticket_tabs.find('.tab.tab-comments').addClass('shown');
        }
        else if ($(this).hasClass('tab-ticket-details-button')) {
            ticket_tabs.find('.tab.tab-ticket-details').addClass('shown');
        }
    });

    // Datepicker
    $(".datepicker-from").datepicker({
        dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
        changeMonth: true,
        changeYear: true,
        prevText: "",
        nextText: "",
        showAnim: '',
        beforeShow: function (el, obj) {
            $(el).addClass('open');
        },
        onClose: function (selectedDate) {
            $(this).removeClass('open');
            $(this).next().datepicker("option", "minDate", selectedDate);
            $(this).next().datepicker("option", "defaultDate", selectedDate);
        }
    });
    $(".datepicker-to").datepicker({
        dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
        changeMonth: true,
        changeYear: true,
        prevText: "",
        nextText: "",
        showAnim: '',
        beforeShow: function (el, obj) {
            $(el).addClass('open');
            var right = $(window).width() - ($(el).offset().left + $(el).outerWidth());
            $('#ui-datepicker-div').addClass('datepicker-to').css({
                'right': right
            });
        },
        onClose: function (selectedDate) {
            $(this).removeClass('open');
            $(this).prev().datepicker("option", "maxDate", selectedDate);
            $('#ui-datepicker-div').removeClass('datepicker-to').css({
                'right': 'auto'
            });
        }
    });
    $.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset}});
    $(window).resize(function () {
        $('.datepicker').datepicker("hide");
    });

    // Ticketing window search section
    $('.advanced-search').click(function () {
        var span = $(this).children('span').find('span:first-child');
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            span.text('Advanced Search');
        }
        else {
            $(this).addClass('open');
            span.text('Hide Advanced Search');
        }
        $(this).parent().toggleClass('open');
        $(this).next().slideToggle();
    });

    // Tag it (Search input tags)
    $('.search-tagit').tagit({
        autocomplete: {
            delay: 0,
            minLength: 9999
        },
        placeholderText: 'Search Summary',
        afterTagAdded: function (event, ui) {
            $('.tagit-new').find('input').attr('placeholder', '');
            $('.search-form .clear-search').show();
        },
        singleField: true,
        singleFieldNode: $('.summary_hidden_input')
    });
    $('.tagit-new').focusin(function () {
        var container = $(this).parent();
        container.addClass('focused');
        $(this).focusout(function(){
            container.removeClass('focused');
        })
    });
    $('.search-tagit .tagit-new input').attr('id', 'si-summary');
    $('.search-form .clear-search').click(function () {
        $(this).parent().find(".search-tagit").tagit("removeAll");
        $(this).parent().find('.tagit-new').find('input').attr('placeholder', 'Search Summary');
        $(this).hide();
    });

    // Select CSS changes
    $('select.input-box').each(function(){
        var isDisabled = $(this).children('option:first-child').is(':disabled');
        if (isDisabled){
            $(this).css('color', '#CCCCCC');
        }
        $(this).change(function(){
            $(this).css('color', '#4A4A4A');
        })
    });

    // Ticketing window new ticket function
    var ticket_list = $('.ticket-list-container .ticket-list');
    var new_ticket = ticket_list.find('.ticket:first-child');
    function show_new_ticket(){
        if(ticket_list.scrollTop() == 0){
            new_ticket.hide().css('opacity', '0').addClass('transition new').slideDown();
            transition(400);
        }
        else{
            ticket_list.animate({ scrollTop: 0 }, 200);
            setTimeout(function(){
                new_ticket.hide().css('opacity', '0').addClass('transition new').slideDown();
            }, 400);
            transition(1000);
        }
        function transition(time){
            setTimeout(function(){
                new_ticket.css('opacity', '1');
                setTimeout(function(){
                    new_ticket.removeClass('new');
                    setTimeout(function(){
                        new_ticket.removeClass('transition');
                    }, 1000);
                }, 2000);
            }, time);
        }
    }

    // Ticketing window new comment function
    var tab_comments = $('.ticket-tabs .tab-comments');
    var new_comment = tab_comments.find('li:first-child');
    function show_new_comment(){
        tab_button.removeClass('active');
        tab.removeClass('shown');
        $('.tab-button.tab-comments-button').addClass('active');

        tab_comments.addClass('shown');
        comment_input.trigger('focusin').trigger('focusout');

        if(!isMobile){
            if(tab_comments.scrollTop() == 0){
                new_comment.hide().css('opacity', '0').addClass('transition new').slideDown();
                transition(400);
            }
            else{
                tab_comments.animate({ scrollTop: 0 }, 200);
                setTimeout(function(){
                    new_comment.hide().css('opacity', '0').addClass('transition new').slideDown();
                }, 400);
                transition(1000);
            }
        }

        var ticket_info_offset = $(".ticket-info").offset().top;
        if(isMobile){
            if($('body').scrollTop() == ticket_info_offset){
                new_comment.hide().css('opacity', '0').addClass('transition new').slideDown();
                transition(400);
            }
            else{
                $('body').animate({ scrollTop: ticket_info_offset }, 200);
                setTimeout(function(){
                    new_comment.hide().css('opacity', '0').addClass('transition new').slideDown();
                }, 400);
                transition(1000);
            }
        }

        function transition(time){
            setTimeout(function(){
                new_comment.css('opacity', '1');
                setTimeout(function(){
                    new_comment.removeClass('new');
                    setTimeout(function(){
                        new_comment.removeClass('transition');
                    }, 1000);
                }, 2000);
            }, time);
        }
    }

    // Navigating sliding Pages
    // Ticketing window
    $('ul.ticket-list>li>a').click(function () {
        tickets.go_to_page('ticket_detail');
        setTimeout(function(){
            if(isMobile){
                $('.comment-input-container').addClass('fixed');
            }}, 500);
    });
    $('.post-comment-button').click(function () {
        $(this).prev().val('');
        show_new_comment();
    });
    $('button.new-ticket-button').click(function () {
        tickets.go_to_page('new_ticket');
    });
    $('.new-ticket-type-selection .nt-report-problem').click(function () {
        tickets.go_to_page('ticket_report_problem');
    });
    $('.new-ticket-type-selection .nt-request-change').click(function () {
        tickets.go_to_page('ticket_request_change');
    });
    $('.new-ticket-type-selection .nt-get-assistance').click(function () {
        tickets.go_to_page('ticket_get_assistance');
    });
    $('.new-ticket-type-selection .nt-schedule-visit').click(function () {
        tickets.go_to_page('ticket_schedule_visit');
    });
    $('.new-ticket-form .cancel-button').click(function () {
        tickets.go_to_page('new_ticket', 'right');
    });
    $('.new-ticket-form .submit-button').click(function () {
        tickets.go_to_page('main');
        new_ticket.hide();
        setTimeout(function(){
            show_new_ticket();
        }, 500);
    });
    $('.nav-window.ticketing-window').children('header').find('.search-button').click(function () {
        tickets.go_to_page('search_ticket');
    });
    $('.nav-window.ticketing-window .search-form .search-button').click(function () {
        if($(this).closest('.search-form').find('#si_summary_hidden_input').val() == ''){
            tickets.go_to_page('search_no_results');
        }
        else{
            tickets.go_to_page('search_results');
        }
    });
    $('.nav-window.ticketing-window .search-form .cancel-button').click(function () {
        tickets.go_to_page('main', 'right');
    });
    $('.nav-window.ticketing-window .search-no-results .search-button').click(function () {
        tickets.go_to_page('search_ticket', 'right');
    });

    // Launchboard window
    $('.productivity-button').click(function () {
        launchboard.go_to_page('productivity_links', 'right');
    });
    $('.administrative-button').click(function () {
        launchboard.go_to_page('administrative_links');
    });
    $('.link-recovery-cloud').click(function () {
        launchboard.go_to_page('recover_cloud_links');
    });

    // Sungard window
    $('.nav-window.impersonate-window .user-list .company.with-sub').click(function () {
        sungard.go_to_page('user_sub_list');
        $(this).closest('.nav-window').find('.selected-company span').text($(this).children('.company-name').text());
        $(this).closest('.nav-window').find('.input-container input[type="search"]').val('');
    });

    // Events window
    $('.nav-window.events-window').children('header').find('.search-button').click(function () {
        events.go_to_page('search_events');
    });
    $('.nav-window.events-window .search-form .search-button').click(function () {
        if($(this).closest('.search-form').find('#ev_summary_hidden_input').val() == ''){
            events.go_to_page('search_no_results');
        }
        else{
            events.go_to_page('search_results');
        }
    });
    $('.nav-window.events-window .search-form .cancel-button').click(function () {
        events.go_to_page('events_list', 'right');
    });
    $('.nav-window.events-window .search-no-results .search-button').click(function () {
        events.go_to_page('search_events', 'right');
    });



    var nav_button = $('.nav-dropdown-button');
    var animation_speed = 150;

    // On nav button click
    nav_button.click(function () {
        var button = $(this);
        var data_name = $(this).attr('data-name');
        var nav_window = $(this).parent().find('.nav-window');

        if (nav_window.is(':visible')) {
            nav_window.fadeOut(animation_speed).removeClass('shown');
            button.removeClass('active');
            $('#main-header').removeClass('nav-open');
        } else {
            $('.nav-window').fadeOut(animation_speed);
            $('.nav-dropdown-button').removeClass('active');
            nav_window.fadeIn(animation_speed).addClass('shown');
            button.addClass('active');
            $('#main-header').addClass('nav-open');
            setTimeout(reset_nav_windows, animation_speed);

            // Adjusting position of the up arrow on the left side nav windows
            $('.nav-window-arrow-positioning').addClass('old');
            setTimeout(function(){ $('.nav-window-arrow-positioning.old').remove() }, animation_speed);
            if($(this).closest('nav.pull-left').length){
                var window_offset = $(this).parent().children('.nav-window').offset();
                var this_offset = $(this).offset();
                var window_left = parseFloat(window_offset.left);
                var this_left = parseFloat(this_offset.left);
                var this_width = parseFloat($(this).outerWidth());
                $('head').append('<style class="nav-window-arrow-positioning">' +
                    '.nav-window.' + $(this).parent().children('.nav-window').attr('data-name') + ' header:before{left:' +
                    ((this_left - window_left) + this_width + 1) + 'px;}</style>');
            }
        }

        // Adjusting position of the hover tooltips on header
        var button_text = nav_window.children('header').find('button span');
        button_text.each(function () {
            $(this).show();
            var width_button = $(this).parent().outerWidth();
            var width = $(this).outerWidth();
            $(this).css('left', -((width - width_button) / 2) + 'px').hide();
        });


        setTimeout(function(){
            switch(data_name){
                case 'tickets':
                    tickets.go_to_page('main','left',true);
                    break;
                case 'launchboard':
                    launchboard.go_to_page('productivity_links','left',true);
                    break;
                case 'sungard':
                    sungard.go_to_page('user_list','left',true);
                    break;
                case 'events':
                    events.go_to_page('events_list','left',true);
                    break;

            }
        }, animation_speed);

        switch(data_name){
            case 'sungard':
                $('.nav-window.impersonate-window').find('.input-container input[type="search"]').val('');
                $('.nav-window.impersonate-window').find('.nav-window-content').children('.search-results, .search-no-results').hide();
                break;
        }

    });

    // On outside nav button click
    $(document).mouseup(function (e) {
        if (!$('.nav-window, #ui-datepicker-div').is(e.target) && $('.nav-dropdown-button').has(e.target).length==0 && !$('.nav-dropdown-button').is(e.target)
            && $('.nav-window, #ui-datepicker-div').has(e.target).length === 0) {
            $('.nav-window.shown').fadeOut(animation_speed, function() {
                $('.nav-window').removeClass('shown');
                $('.nav-dropdown-button').removeClass('active');
                $('#main-header').removeClass('nav-open');
                reset_nav_windows();
            });
        }
    });

    // Reset every nav window
    function reset_nav_windows(){
        tickets.go_to_page('main','left',true);
        launchboard.go_to_page('productivity_links','right',true);
        sungard.go_to_page('user_list','right',true);
        events.go_to_page('events_list');
        $('.nav-window.impersonate-window').find('.input-container input[type="search"]').val('');

        $('.nav-window .clear-search').click();
    }

    // Sungard window search input
    $('.nav-window.impersonate-window').find('input[type="search"]').on("change keyup paste", function(){
        if($(this).val().length){
            $(this).closest('.nav-window-content').children('.search-results').show();
            $(this).closest('.input-container').addClass('border');
        }
        else{
            $(this).closest('.nav-window-content').children('.search-results').hide();
            $(this).closest('.input-container').removeClass('border');
        }
        if($(this).val() == 'no'){
            $(this).closest('.nav-window-content').children('.search-no-results').show();
            $(this).closest('.nav-window-content').children('.search-results').hide();
            $(this).closest('.input-container').removeClass('border');
        }
        else{
            $(this).closest('.nav-window-content').children('.search-no-results').hide();
        }
    });

    // Sungard window info hover
    var icon_mouse_in;
    var tooltip_mouse_in;
    var delay;
    $('.nav-window.impersonate-window .user-list').find('.icon').mouseenter(function () {
        clearTimeout(delay);
        $('.info-clone').remove();
        var info = $(this).children('.info');
        var info_clone = info.clone().appendTo('body');
        info_clone.addClass('info-clone');
        info_clone.css({
            'top': $(this).offset().top + 20,
            'left': $(this).offset().left - 94
        }).show();

        icon_mouse_in = true;

        info_clone.mouseenter(function () {
            clearTimeout(delay);
            tooltip_mouse_in = true;
        }).mouseleave(function () {
            tooltip_mouse_in = false;
            if(!icon_mouse_in && !tooltip_mouse_in){
                clearTimeout(delay);
                $('.info-clone').remove();
            }
        });


        $(this).mouseleave(function () {
            icon_mouse_in = false;
            delay = setTimeout(function() {
                if(!icon_mouse_in && !tooltip_mouse_in){
                    $('.info-clone').remove();
                }
            }, 100);
        })

    });

    // Sungard window user click
    $('.nav-window.impersonate-window .user-list').find('.user').click(function () {
        $(this).closest('.button-container').children('.sungard-button').text($(this).find('.user-info p:first-child').text());
        $('.nav-dropdown-button.sungard-button').click();
    })


});

// Navigating sliding Pages

var ticket_page_list = {
    root: {
        title: 'Root',
        element: ''
    }
    ,
    main: {
        title: 'Tickets',
        element: '.ticketing-window .ticket-list-container',
        onShow: function ($el) {
            $el.find('header .refresh-button').show();
        },
        onLeave: function ($el) {
            $el.find('header .refresh-button').hide();
        }
    }
    ,
    ticket_detail: {
        title: 'Ticket Detail',
        element: '.ticketing-window .ticket-content-container',
        parent: 'main',
        onShow: function ($el) {
            $el.find('header .refresh-button').show();
        },
        onLeave: function ($el) {
            $el.find('header .refresh-button').hide();
        }
    }
    ,
    new_ticket: {
        title: 'New Ticket',
        element: '.ticketing-window .new-ticket-type-selection',
        parent: 'main',
        onShow: function ($el) {
            $el.children('header').find('.new-ticket-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.new-ticket-button').removeClass('active');
        }
    },
    ticket_report_problem: {
        title: 'Report a Problem',
        element: '.ticketing-window .new-ticket-form.report-problem-form',
        parent: 'new_ticket',
        onShow: function ($el) {
            $el.children('header').find('.new-ticket-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.new-ticket-button').removeClass('active');
        }
    },
    ticket_request_change: {
        title: 'Request a Change',
        element: '.ticketing-window .new-ticket-form.request-change-form',
        parent: 'new_ticket',
        onShow: function ($el) {
            $el.children('header').find('.new-ticket-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.new-ticket-button').removeClass('active');
        }
    },
    ticket_get_assistance: {
        title: 'Get Assistance',
        element: '.ticketing-window .new-ticket-form.get-assistance-form',
        parent: 'new_ticket',
        onShow: function ($el) {
            $el.children('header').find('.new-ticket-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.new-ticket-button').removeClass('active');
        }
    },
    ticket_schedule_visit: {
        title: 'Schedule a Visit',
        element: '.ticketing-window .new-ticket-form.schedule-visit-form',
        parent: 'new_ticket',
        onShow: function ($el) {
            $el.children('header').find('.new-ticket-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.new-ticket-button').removeClass('active');
        }
    },
    search_ticket: {
        title: 'Search',
        element: '.ticketing-window .search-form',
        parent: 'main',
        onShow: function ($el) {
            $el.children('header').find('.search-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.search-button').removeClass('active');
        }
    },
    search_results: {
        title: 'Search Results',
        element: '.ticketing-window .search-results',
        parent: 'search_ticket',
        onShow: function ($el) {
            $el.children('header').find('.search-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.search-button').removeClass('active');
        }
    },
    search_no_results: {
        title: 'Search Results',
        element: '.ticketing-window .search-no-results',
        parent: 'search_ticket',
        onShow: function ($el) {
            $el.children('header').find('.search-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.search-button').removeClass('active');
        }
    }
};
var recovery_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    recovery_links: {
        title: 'Recovery',
        element: '.links-list.recovery-links',
        onShow: function ($el) {
            $el.children('header').find('.recovery-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.recovery-button').removeClass('active');
        }
    }
};
var lacunhboard_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    productivity_links: {
        title: 'Productivity',
        element: '.links-list.productivity-links',
        onShow: function ($el) {
            $el.children('header').find('.productivity-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.productivity-button').removeClass('active');
        }
    },
    administrative_links: {
        title: 'Administrative',
        element: '.links-list.administrative-links',
        onShow: function ($el) {
            $el.children('header').find('.administrative-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.administrative-button').removeClass('active');
        }
    },
    recover_cloud_links: {
        title: 'Recover to Cloud',
        parent: 'productivity_links',
        element: '.links-list.recover-cloud-links',
        onShow: function ($el) {
            $el.children('header').find('button').hide();
        },
        onLeave: function ($el) {
            $el.children('header').find('button').show();
        }
    }
};
var impersonate_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    user_list: {
        title: 'Impersonate User',
        element: '.nav-window.impersonate-window .user-list.main',
        onShow: function ($el) {
            setTimeout(function(){ $el.children('.nav-window-content').children('.search-results').slideUp(300) }, 300);
        },
        onLeave: function ($el) {
        }
    },
    user_sub_list: {
        title: 'Impersonate',
        parent: 'user_list',
        element: '.nav-window.impersonate-window .user-list.sub-list',
        onShow: function ($el) {
            setTimeout(function(){ $el.find('.selected-company').slideDown(300); }, 300);
        },
        onLeave: function ($el) {
            setTimeout(function(){ $el.find('.selected-company').slideUp(300); }, 300);
        }
    }
};
var events_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    events_list: {
        title: 'Events',
        element: '.nav-window.events-window .events-list-container',
        onShow: function ($el) {
        },
        onLeave: function ($el) {
        }
    },
    search_events: {
        title: 'Search',
        parent: 'events_list',
        element: '.nav-window.events-window .search-form',
        onShow: function ($el) {
            $el.children('header').find('.search-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.search-button').removeClass('active');
        }
    },
    search_results: {
        title: 'Search',
        parent: 'search_events',
        element: '.nav-window.events-window .search-results',
        onShow: function ($el) {
        },
        onLeave: function ($el) {
        }
    },
    search_no_results: {
        title: 'Search',
        parent: 'search_events',
        element: '.nav-window.events-window .search-no-results',
        onShow: function ($el) {
        },
        onLeave: function ($el) {
        }
    }
};
var user_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    profile_tab: {
        title: 'Profile',
        element: '.nav-window.user-window .profile-tab',
        onShow: function ($el) {
            $el.find('.profile-tab').children('header').children('h3').hide();
            $el.find('.profile-tab').children('header').children('.user-name').show();
        },
        onLeave: function ($el) {
        }
    }
};

function MovePages(page_list, $el) {
    var that = this;
    var $nav_window = $el;
    var $tabs = $el.find('.nav-window-content .sliding-tab');
    var current_page = 'root';
    var animating = false;
    this.go_to_page = function (page, direction, force) {
        var offset = $el.find('.nav-window-content').width();
        if (page == current_page) return false;
        if (animating && !force) return false;
        animating = true;
        var _direction = (direction == 'right') ? -1 : 1;
        var $target = $(page_list[page].element);
        var $source = $(page_list[current_page].element);
        //show/hide
        $tabs.hide().css({position: 'absolute'});
        $target.show().css({position: 'relative'});
        $('.nav-window .nav-window-content').addClass('overflow-hidden');
        $source.show();
        //init target position
        $target.transition({x: _direction * offset}, 0);
        //move
        $target.transition({x: 0},function() {
            animating = false;
        });
        $source.transition({x: -_direction * offset},function(){
           if($source.queue().length<=1) $source.hide();
            $target.css('transform', 'none').find('.form-buttons').addClass('fixed');
            $('.nav-window .nav-window-content').removeClass('overflow-hidden');
        });
        //header title&click
        var $header = $nav_window.find('header>h3');
        $header.off('click');
        if (page_list[page].parent) {
            $header.find('>span').text(page_list[page_list[page].parent].title);
            $header.addClass('back');
            $header.click(function () {
                that.go_to_page(page_list[page].parent, 'right')
            })
        } else {
            $header.find('>span').text(page_list[page].title);
            $header.removeClass('back');
        }
        //trigger events
        if (page_list[current_page].onLeave) {
            page_list[current_page].onLeave($el);
        }
        if (page_list[page].onShow) {
            page_list[page].onShow($el);
        }
        //set current page
        current_page = page;
    };
}

var tickets = new MovePages(ticket_page_list, $('.ticketing-window'));
var recovery = new MovePages(recovery_page_list, $('.recovery-window'));
var launchboard = new MovePages(lacunhboard_page_list, $('.launchboard-window'));
var sungard = new MovePages(impersonate_page_list, $('.impersonate-window'));
var events = new MovePages(events_page_list, $('.events-window'));
var user = new MovePages(user_page_list, $('.user-window'));
tickets.go_to_page('main');
recovery.go_to_page('recovery_links');
launchboard.go_to_page('productivity_links');
sungard.go_to_page('user_list');
events.go_to_page('events_list');
user.go_to_page('profile_tab');



$(window).load(function () {
    $('.nav-dropdown-button.user-button').click();
});


// Create dummy element
$('body').append('<div class="nav-window ticketing-window dummy-container"><div class="dummy"></div></div>');