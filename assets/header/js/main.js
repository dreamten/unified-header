// Detect window width
var isMobile = false;
function check_window_width() {
    var window_width = $(window).width();
    if (window_width < 768) {
        $('body').addClass('mobile');
        isMobile = true;
    }
    else {
        $('body').removeClass('mobile');
        isMobile = false;
    }
    return window_width;
}
check_window_width();
var check = true;
$(window).resize(function () {
    if (check) {
        check = false;
        setTimeout(function () {
            check_window_width();
            check = true
        }, 100);
    }
});

// Detect IE
function isIE( version, comparison ){
    var $div = $('<div style="display:none;"/>').appendTo($('body'));
    $div.html('<!--[if '+(comparison||'')+' IE '+(version||'')+']><a>&nbsp;</a><![endif]-->');
    var ieTest = $div.find('a').length;
    $div.remove();
    return ieTest;
}

var container_height_before;
var impersonate_open = false;
var chosen_company;

$(document).ready(function () {
    check_window_width();

    // Remove touch devices tap delay
    $(function () {
        FastClick.attach(document.body);
    });

    // Mobile functions
    var once=false;
    function mobile_edits() {
        var left_nav = $('nav.pull-left');
        if ($(window).width() < 768) {
            left_nav.addClass('nav-window');
            left_nav.children('.content').addClass('nav-window-content');
            left_nav.children('.content').children('div').addClass('sliding-tab');
            
            if (!once) {
                mobile_nav_page_list = {
                    root: {
                        title: 'Root',
                        element: ''
                    },
                    links_dir: {
                        title: 'Menu',
                        element: '.pull-left.nav-window .mobile-links',
                        onShow: function ($el) {
                        },
                        onLeave: function ($el) {
                        }
                    },
                    recovery_window: {
                        parent: 'links_dir',
                        title: 'Recovery',
                        element: '.pull-left.nav-window .portal-window',
                        onShow: function ($el) {
                        },
                        onLeave: function ($el) {
                        }
                    },
                    launchboard_window: {
                        title: 'Launchboard',
                        element: '.pull-left.nav-window .tool-window',
                        onShow: function ($el) {
                        },
                        onLeave: function ($el) {
                        }
                    },
                    sungard_window: {
                        title: 'Sungard AS',
                        element: '.pull-left.nav-window .impersonate-window',
                        onShow: function ($el) {
                            sungard.refresh_header();
                        },
                        onLeave: function ($el) {
                        }
                    }
                };
                mobile_nav = new MovePages(mobile_nav_page_list, $('.pull-left.nav-window'));
                


                $('.pull-left.nav-window .mobile-links .recovery-link').click(function () {
                    mobile_nav.go_to_page('recovery_window');
                });
                $('.pull-left.nav-window .mobile-links .launchboard-link').click(function () {
                    mobile_nav.go_to_page('launchboard_window');
                });
                $('.pull-left.nav-window .mobile-links .sungard-link').click(function () {
                    mobile_nav.go_to_page('sungard_window');
                });
                mobile_nav.go_to_page('links_dir');
                once=true;
            }
            
            mobile_nav.redraw_page();
        }
        else {
            left_nav.removeClass('nav-window').attr("style", "");
            left_nav.children('.content').removeClass('nav-window-content overflow-hidden').attr("style", "");
            left_nav.children('.content').children('div').removeClass('sliding-tab').attr("style", "");
            left_nav.find('.nav-window').attr("style", "");
            sungard.refresh_header();
        }
    }

    mobile_edits();
    var windowWidth = $(window).width();
    $(window).resize(function () {
        if ($(window).width() != windowWidth) {
            windowWidth = $(window).width();
            mobile_edits();
            close_nav();
            reset_tour();
        }
    });

    // Ticketing window refresh button
    var refresh_button = $('.nav-window .refresh-button');
    refresh_button.click(function () {
        var refresh_box = $(this).closest('.nav-window').find('.refresh-box');
        var e = $(this);
        e.addClass('rotate-once');
        refresh_box.slideDown(function () {
            setTimeout(
                function () {
                    refresh_box.slideUp();
                    e.removeClass('rotate-once');
                }, 1000);
        });
    });

    // Ticketing window comment input
    $('.nav-window .comment-input-container textarea').each(function(){
        var comment_input = $(this);
        var ticket_content = comment_input.closest('.ticket-content');
        var container = comment_input.closest('.comment-input-container');
        var container_clone = container.clone().addClass('no-transition').appendTo('.dummy');
        container_height_before = container_clone.outerHeight();
        ticket_content.css('padding-bottom', container_height_before);
        comment_input.focusin(function () {
            var that = $(this);
            container = $(this).closest('.comment-input-container');
            container_clone = container.clone().addClass('no-transition open').appendTo('.dummy');
            var shown_tab = ticket_content.find('.ticket-tabs .tab.shown');
            container.addClass('open');
            var container_height_after = container_clone.outerHeight();
            if($(this).closest('#sidepanel').length){
                $('#sidepanel .ticket-tabs').find('.tab').css('height', comment_window_height - container_height_after);
            }
            else{
                shown_tab.css('max-height', 375 - (container_height_after - container_height_before));
            }
            ticket_content.css('padding-bottom', container_height_after);
            $(document).click(function (e) {
                if ($(e.target).closest(".comment-input-container").length > 0) {
                    return false;
                }
                container.removeClass('open');
                container_clone.remove();
                if(that.closest('#sidepanel').length){
                    $('#sidepanel .ticket-tabs').find('.tab').css('height', comment_window_height - container_height_before);
                }
                else{
                    shown_tab.css('max-height', 375);
                }
                ticket_content.css('padding-bottom', container_height_before);
            });
        });
    });

    // Ticketing window ticket tabs
    var ticket_tabs = $('.ticket-tabs');
    var tab_button = ticket_tabs.find('.tab-button');
    var tab = ticket_tabs.find('.tab');
    ticket_tabs.find('.tab-button').click(function () {
        var parent = $(this).closest('.ticket-tabs');
        parent.find('.tab-button').removeClass('active');
        parent.find('.tab').removeClass('shown');
        $(this).addClass('active');
        if ($(this).hasClass('tab-comments-button')) {
            parent.find('.tab.tab-comments').addClass('shown');
        }
        else if ($(this).hasClass('tab-ticket-details-button')) {
            parent.find('.tab.tab-ticket-details').addClass('shown');
        }
    });

    // Datepicker
    $(".datepicker-from").datepicker({
        dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
        changeMonth: true,
        changeYear: true,
        prevText: "",
        nextText: "",
        showAnim: '',
        beforeShow: function (el, obj) {
            $(el).addClass('open');
            if($('#sidepanel').find('input.datepicker').hasClass('open')){
                $('#ui-datepicker-div').addClass('sidepanel-datepicker');
            }
        },
        onClose: function (selectedDate) {
            if($('#sidepanel').find('input.datepicker').hasClass('open')){
                $('#ui-datepicker-div').removeClass('sidepanel-datepicker');
            }
            $(this).removeClass('open');
            $(this).next().datepicker("option", "minDate", selectedDate);
            $(this).next().datepicker("option", "defaultDate", selectedDate);
        }
    });
    $(".datepicker-to").datepicker({
        dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
        changeMonth: true,
        changeYear: true,
        prevText: "",
        nextText: "",
        showAnim: '',
        beforeShow: function (el, obj) {
            $(el).addClass('open');
            if($('#sidepanel').find('input.datepicker').hasClass('open')){
                $('#ui-datepicker-div').addClass('sidepanel-datepicker');
            }
            var right = $(window).width() - ($(el).offset().left + $(el).outerWidth());
            $('#ui-datepicker-div').addClass('datepicker-to').css({
                'right': right
            });
        },
        onClose: function (selectedDate) {
            if($('#sidepanel').find('input.datepicker').hasClass('open')){
                $('#ui-datepicker-div').removeClass('sidepanel-datepicker');
            }
            $(this).removeClass('open');
            $(this).prev().datepicker("option", "maxDate", selectedDate);
            $('#ui-datepicker-div').removeClass('datepicker-to').css({
                'right': 'auto'
            });
        }
    });
    $.extend($.datepicker, {
        _checkOffset: function (inst, offset, isFixed) {
            return offset
        }
    });
    $(window).resize(function () {
        $('.datepicker').datepicker("hide");
    });

    $(document).on( "mouseup touchstart", function(e){
        if (!$('#ui-datepicker-div').is(e.target) && $('#ui-datepicker-div').has(e.target).length === 0 && !$('.datepicker.open').is(e.target) && $('.datepicker.open').has(e.target).length === 0){
            $(".datepicker").datepicker("hide");
        }
    });

    // Ticketing window search section
    $('.advanced-search').click(function () {
        var span = $(this).children('span').find('span:first-child');
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            span.text('Advanced Search');
        }
        else {
            $(this).addClass('open');
            span.text('Hide Advanced Search');
        }
        $(this).parent().toggleClass('open');
        $(this).next().slideToggle();
    });

    // Tag it (Search input tags)
    $('.search-tagit').each(function(){
        var label;
        var placeholder;
        var parent;
        if($(this).closest('#main-header').length){
            label = 'si-summary';
            placeholder = 'Search Summary';
            parent = $('#main-header');
        }
        else{
            label = 'sidepanel-si-summary';
            placeholder = 'Search Tickets';
            parent = $('#sidepanel');
        }
        $(this).tagit({
            autocomplete: {
                delay: 0,
                minLength: 9999
            },
            placeholderText: placeholder,
            afterTagAdded: function (event, ui) {
                parent.find('.tagit-new').find('input').attr('placeholder', '');
                parent.find('.search-form .clear-search').show();
            },
            singleField: true,
            singleFieldNode: $('.summary_hidden_input')
        });
        parent.find('.tagit-new').focusin(function () {
            var container = $(this).parent();
            container.addClass('focused');
            $(this).focusout(function () {
                container.removeClass('focused');
            })
        });
        parent.find('.search-tagit .tagit-new input').attr('id', label);
        parent.find('.search-form .clear-search').click(function () {
            $(this).parent().find(".search-tagit").tagit("removeAll");
            $(this).parent().find('.tagit-new').find('input').attr('placeholder', placeholder);
            $(this).hide();
        });

    });

    // Select CSS changes
    $('select.input-box').each(function () {
        select_color($(this));
        $(this).change(function () {
            select_color($(this));
        })
    });

    // Navigating sliding Pages
    // Ticketing window
    $('#main-header ul.ticket-list>li>a').click(function () {
        tickets.go_to_page('ticket_detail');
    });
    $('#main-header .post-comment-button').click(function () {
        $(this).prev().val('');
        show_new_comment($('#main-header'));
    });
    $('#main-header button.new-ticket-button').click(function () {
        tickets.go_to_page('new_ticket');
    });
    $('#main-header .new-ticket-type-selection .nt-report-problem').click(function () {
        tickets.go_to_page('ticket_report_problem');
    });
    $('#main-header .new-ticket-type-selection .nt-request-change').click(function () {
        tickets.go_to_page('ticket_request_change');
    });
    $('#main-header .new-ticket-type-selection .nt-get-assistance').click(function () {
        tickets.go_to_page('ticket_get_assistance');
    });
    $('#main-header .new-ticket-type-selection .nt-schedule-visit').click(function () {
        tickets.go_to_page('ticket_schedule_visit');
    });
    $('#main-header .new-ticket-form .cancel-button').click(function () {
        tickets.go_to_page('new_ticket', 'right');
    });
    $('#main-header .new-ticket-form .submit-button').click(function () {
        tickets.go_to_page('main');
        show_new_ticket($('#main-header'));
    });
    $('#main-header .nav-window.ticketing-window').children('header').find('.search-button').click(function () {
        tickets.go_to_page('search_ticket');
    });
    $('#main-header .nav-window.ticketing-window .search-form .search-button').click(function () {
        if ($(this).closest('.search-form').find('#si_summary_hidden_input').val() == '') {
            tickets.go_to_page('search_no_results');
        }
        else {
            tickets.go_to_page('search_results');
        }
    });
    $('#main-header .nav-window.ticketing-window .search-form .cancel-button').click(function () {
        tickets.go_to_page('main', 'right');
    });
    $('#main-header .nav-window.ticketing-window .search-no-results .search-button').click(function () {
        tickets.go_to_page('search_ticket', 'right');
    });

    // Launchboard window
    $('.productivity-button').click(function () {
        launchboard.go_to_page('productivity_links', 'right');
    });
    $('.administrative-button').click(function () {
        launchboard.go_to_page('administrative_links');
    });
    $('.link-recovery-cloud').click(function () {
        launchboard.go_to_page('recover_cloud_links');
    });

    // Sungard window add users button to companies
    $('.nav-window.impersonate-window .user-list.main').find('.company.with-sub').closest('li').each(function(){
        $(this).append('<button class="users-button">Users</button>')
    });
    // Sungard window
    $('.nav-window.impersonate-window .user-list li .users-button').click(function () {
        var company_name = $(this).closest('li').find('.company-name').attr('data-name');
        $('.nav-window.impersonate-window').find('.user-list.sub-list').attr('data-name', company_name);
        if(!impersonate_open){
            chosen_company = company_name;
            sungard.go_to_page('user_sub_list');
        }
    });

    // Events window
    $('.nav-window.events-window').children('header').find('.search-button').click(function () {
        events.go_to_page('search_events');
    });
    $('.nav-window.events-window .search-form .search-button').click(function () {
        if ($(this).closest('.search-form').find('#ev_summary_hidden_input').val() == '') {
            events.go_to_page('search_no_results');
        }
        else {
            events.go_to_page('search_results');
        }
    });
    $('.nav-window.events-window .search-form .cancel-button').click(function () {
        events.go_to_page('events_list', 'right');
    });
    $('.nav-window.events-window .search-no-results .search-button').click(function () {
        events.go_to_page('search_events', 'right');
    });

    // User info window
    $('.nav-window.user-window .profile-button').click(function () {
        user.go_to_page('profile_tab', 'right');
    });
    $('.nav-window.user-window .contacts-button').click(function () {
        user.go_to_page('contacts_tab');
    });

    $('.nav-window.user-window .user-edit-button').click(function () {
        user_profile.go_to_page('user_info_edit');

    });
    $('.nav-window.user-window .user-info-edit .cancel-button').click(function () {
        user_profile.go_to_page('user_info', 'right');
    });
    $('.nav-window.user-window .user-info-edit .save-button').click(function () {
        user_profile.go_to_page('user_info');
    });
    $('.nav-window.user-window .change-password').click(function () {
        $('.nav-window.user-window').find('#password-notification').slideDown('300');
        setTimeout(function () {
            $('.nav-window.user-window').find('#password-notification').slideUp('300');
        }, 5000);
    });
    $('.nav-window.user-window .user-password-edit .cancel-button').click(function () {
        user_profile.go_to_page('user_info', 'right');
    });
    $('.nav-window.user-window .user-password-edit .save-button').click(function () {
        user_profile.go_to_page('user_info');
    });

    // Tour info window
    $('.nav-window.tour-window .information-button').click(function () {
        tour.go_to_page('info_tab', 'right');
    });
    $('.nav-window.tour-window .tutorials-button').click(function () {
        tour.go_to_page('tutorials_tab');
    });

    // Widget
    $('.nav-window.widget-window header .server-button').click(function () {
        r2cwidget.go_to_page('overview_tab');
    });
    $('.nav-window.widget-window header .volume-button').click(function () {
        r2cwidget.go_to_page('volume_tab');
    });
    $('.nav-window.widget-window header .filter-button').click(function () {
        r2cwidget.go_to_page('filter_tab');
    });
    $('.nav-window.widget-window header .report-button').click(function () {
        r2cwidget.go_to_page('report_tab');
    });

    // Widget Reports
    $('.nav-window.widget-window .report-tab .quick-reports-button').click(function () {
        $('.report-tab').find('.wide-header-button').removeClass('active');
        $(this).addClass('active');
        r2cwidget_report.go_to_page('quick_reports_tab', 'right');
    });
    $('.nav-window.widget-window .report-tab .reports-by-date-button').click(function () {
        $('.report-tab').find('.wide-header-button').removeClass('active');
        $(this).addClass('active');
        r2cwidget_report.go_to_page('reports_by_date_tab');
    });


    var nav_button = $('.nav-dropdown-button');
    var animation_speed = 150;

    // On nav button click
    nav_button.click(function () {
        var button = $(this);
        var data_name = $(this).attr('data-name');
        var nav_window = $(this).parent().children('.nav-window');

        if (!tour_on || $(this).hasClass('tour-button')) {

            if (nav_window.is(':visible')) {
                nav_window.fadeOut(animation_speed).removeClass('shown');
                button.removeClass('active');
                $('#main-header').removeClass('nav-open');
                setTimeout(reset_nav_windows, animation_speed);
            } else {
                $('.nav-window.shown').fadeOut(animation_speed, function () {
                    $('.nav-window').removeClass('shown');
                    reset_nav_windows();
                });
                $('.nav-dropdown-button').removeClass('active');
                nav_window.fadeIn(animation_speed, function () {
                    nav_window.addClass('shown');
                });
                button.addClass('active');
                $('#main-header').addClass('nav-open');

                // Adjusting position of the up arrow on the left side nav windows
                $('.nav-window-arrow-positioning').addClass('old');
                setTimeout(function () {
                    $('.nav-window-arrow-positioning.old').remove()
                }, animation_speed);
                if ($(this).closest('nav.pull-left').length) {
                    var window_offset = $(this).parent().children('.nav-window').offset();
                    var this_offset = $(this).offset();
                    var window_left = parseFloat(window_offset.left);
                    var this_left = parseFloat(this_offset.left);
                    var this_width = parseFloat($(this).outerWidth());
                    if(data_name == "sungard"){
                        $('head').append('<style class="nav-window-arrow-positioning">' +
                            '.nav-window.' + $(this).parent().children('.nav-window').attr('data-name') + ' header:before{left:' +
                            ((this_left - window_left) + this_width - 13) + 'px;}</style>');
                    }
                    else{
                        $('head').append('<style class="nav-window-arrow-positioning">' +
                            '.nav-window.' + $(this).parent().children('.nav-window').attr('data-name') + ' header:before{left:' +
                            ((this_left - window_left) + this_width + 1) + 'px;}</style>');
                    }
                }

                // Widget fade
                if ($(this).hasClass('r2c-widget')) {
                    $(this).find('.summary').fadeOut(300);
                }
            }

            setTimeout(function () {
                switch (data_name) {
                    case 'tickets':
                        tickets.go_to_page('main', 'left', true);
                        break;
                    case 'launchboard':
                        launchboard.go_to_page('productivity_links', 'left', true);
                        break;
                    case 'sungard':
                        break;
                    case 'events':
                        events.go_to_page('events_list', 'left', true);
                        break;

                }
            }, animation_speed);

            switch (data_name) {
                case 'sungard':
                    break;
            }
        }
    });

    // Adjusting position of the hover tooltips on header
    $('.pull-right').find('button.has-tooltip').on("mouseenter", function () {
        var width_button = $(this).outerWidth();
        var tooltip = $(this).find('.button-tooltip');
        var width_tooltip = tooltip.outerWidth();
        if(width_tooltip > 0){
            tooltip.css('left', -((width_tooltip - width_button) / 2) + 'px').hide();
        }
    });

    // On outside nav button click
    $(document).on( "mouseup touchstart", function(e){
        if (!isMobile && !$('.nav-window, #ui-datepicker-div').is(e.target) && $('.nav-dropdown-button').has(e.target).length == 0 && !$('.nav-dropdown-button').is(e.target)
            && $('.nav-window, #ui-datepicker-div').has(e.target).length === 0 && !$('.info-clone-container, .info-clone-container *').is(e.target) && !$('.mfp-wrap, .mfp-wrap *').is(e.target)) {
            $('.nav-window.shown').fadeOut(animation_speed, function () {
                $('.nav-window').removeClass('shown');
                $('.nav-dropdown-button').removeClass('active');
                $('#main-header').removeClass('nav-open');
                reset_nav_windows();
            });
        }
    });

    // Reset every nav window
    function reset_nav_windows() {
        tickets.go_to_page('main', 'left', true);
        launchboard.go_to_page('productivity_links', 'right', true);
        events.go_to_page('events_list');
        $('.nav-window .clear-search').click();
        user.go_to_page('profile_tab');
        user_profile.go_to_page('user_info');
        $('.contacts-tab').find('input').val('');
        if (mobile_nav) {
            mobile_nav.go_to_page('links_dir');
        }
        tour.go_to_page('info_tab');

        // Widget
        $('.r2c-widget').find('.summary').fadeIn(300);
        $('.asset-list-container').find('.close-button').click();
        r2cwidget.go_to_page('overview_tab');
    }

    // Close and deactivate nav elements
    function close_nav() {
        $('.nav-window.shown').fadeOut(animation_speed, function () {
            $('.nav-window.shown').removeClass('shown');
            $('.nav-dropdown-button').removeClass('active');
            $('#main-header').removeClass('nav-open');
            $('.mobile-toggle').removeClass('active');
            reset_nav_windows();
        });
    }


    // Sungard window search input
    $('.nav-window.impersonate-window').find('input[type="search"]').on("change keyup paste", function () {
        if ($(this).val().length) {
            $(this).closest('.nav-window-content').children('.search-results').show();
            $(this).closest('.input-container').addClass('border');
        }
        else {
            $(this).closest('.nav-window-content').children('.search-results').hide();
            $(this).closest('.input-container').removeClass('border');
        }
        if ($(this).val() == 'no') {
            $(this).closest('.nav-window-content').children('.search-no-results').show();
            $(this).closest('.nav-window-content').children('.search-results').hide();
            $(this).closest('.input-container').removeClass('border');
        }
        else {
            $(this).closest('.nav-window-content').children('.search-no-results').hide();
        }
    });

    // Sungard window user click
    $('.nav-window.impersonate-window .user-list').find('.user, .company').click(function () {
        $(this).closest('.button-container').children('.sungard-button').empty();
        $(this).closest('.pull-left').find('.mobile-links').find('.sungard-link').empty();

        var text_button = $(this).closest('.button-container').children('.sungard-button');
        var text_button_mobile = $(this).closest('.pull-left').find('.mobile-links').find('.sungard-link');

        if($(this).hasClass('user')){
            var parent_company = $(this).find('span.company-name');
            text_button.append('<span class="user-name">' + $(this).find('p.name').attr('data-name') + '</span>');
            text_button_mobile.append('<span class="user-name">' + $(this).find('p.name').attr('data-name') + '</span>');

            if(parent_company.length){
                text_button.append('<span class="company-name">, ' + $(this).find('span.company-name').attr('data-company-name') + '</span>');
                text_button_mobile.append('<span class="company-name">, ' + $(this).find('span.company-name').attr('data-company-name') + '</span>');
            }
            else if($(this).closest('.user-list').hasClass('sub-list')){
                text_button.append('<span class="company-name">, ' + $('.nav-window.impersonate-window').find('.user-list.sub-list').attr('data-name') + '</span>');
                text_button_mobile.append('<span class="company-name">, ' + $('.nav-window.impersonate-window').find('.user-list.sub-list').attr('data-name') + '</span>');
            }

        }
        else{
            text_button.text($(this).find('p.name').attr('data-name'));
            text_button_mobile.text($(this).find('p.name').attr('data-name'));
        }
        close_nav();
    });

    // Sungard chosen company click
    $('.nav-window.impersonate-window .selected-company').click(function(){
        $('.nav-window.impersonate-window .user-list.main').find('.company-name[data-name="' + $(this).find('span').attr('data-name') + '"]').closest('.company').click();
    });

    // Sungard window info hover
    var icon_mouse_in;
    var tooltip_mouse_in;
    var delay;
    $('.nav-window.impersonate-window .user-list').find('.user, .company').children('.icon').mouseenter(function () {
        if (!isMobile) {
            $('.info-clone').remove();
            var info = $(this).children('.info');
            var info_clone = info.clone().appendTo('body');
            info_clone.addClass('info-clone');
            info_clone.css({
                'top': $(this).offset().top + 20,
                'left': $(this).offset().left - 94
            }).show();

            icon_mouse_in = true;

            info_clone.mouseenter(function () {
                clearTimeout(delay);
                tooltip_mouse_in = true;
            }).mouseleave(function () {
                tooltip_mouse_in = false;
                if (!icon_mouse_in && !tooltip_mouse_in) {
                    clearTimeout(delay);
                    $('.info-clone').remove();
                }
            });

            $(this).mouseleave(function () {
                icon_mouse_in = false;
                delay = setTimeout(function () {
                    if (!icon_mouse_in && !tooltip_mouse_in) {
                        $('.info-clone').remove();
                    }
                }, 100);
            })
        }
    });
    $('.nav-window.impersonate-window .user-list .user').find('.icon').click(function (e) {
        if (isMobile) {
            e.stopPropagation();

            $('.info-clone-container').remove();
            var info = $(this).children('.info');
            var info_clone = info.clone().appendTo('body');
            info_clone.wrap('<div class="info-clone-container"></div>');
            var content = $(this).closest('.impersonate-window').children('.nav-window-content');
            info_clone.append('<button class="close-button"></button>');
            info_clone.addClass('info-clone').fadeIn();
            content.addClass('low-opacity');

            info_clone.find('.close-button').click(function () {
                $('.info-clone-container').fadeOut();
                content.removeClass('low-opacity');
            })
        }
    });

    // Info Window Pie Charts
    if(!isIE(8,'lte')){ // Excluding IE8 so they load the header without errors
        $('.nav-window.tour-window .volumes').find('.volume').each(function () {
            var volume_capacity = parseFloat($(this).attr('data-capacity'));
            var used_space = parseFloat($(this).attr('data-used-space'));

            var dataset = [
                {label: 'Used Space', count: used_space},
                {label: 'Free Space', count: volume_capacity - used_space}
            ];
            var dataset_border = [
                {label: '', count: volume_capacity}
            ];

            var color1, color2;
            color1 = '#9B9B9B';
            color2 = '#F1F1F1';

            if($(this).closest('.line').hasClass('healthy')){
                color1 = '#17945E';
                color2 = '#17945E';
            }
            else if($(this).closest('.line').hasClass('weak')){
                color1 = '#F8AF19';
                color2 = '#F8AF19';
            }
            else if($(this).closest('.line').hasClass('poor')){
                color1 = '#EA4F02';
                color2 = '#EA4F02';
            }
            else if($(this).closest('.line').hasClass('exception')){
                color1 = '#5785A7';
                color2 = '#5785A7';
            }


            var pie_width = 19;
            if($(this).hasClass('smallest')){
                pie_width = 14;
            }
            else if($(this).hasClass('largest')){
                pie_width = 35;
            }

            if($(this).closest('.info-color').length){
                pie_width = 35;
            }
            var pie_height = pie_width;
            var radius = Math.floor(pie_width / 2);
            var pie_border_width = 1;

            var color = d3.scale.ordinal()
                .range([color1, color2]);

            var svg = d3.select(this)
                .append('svg')
                .attr('width', Math.ceil(pie_width))
                .attr('height', Math.ceil(pie_height))
                .append('g')
                .attr('transform', 'translate(' + (pie_width / 2) + ',' + (pie_height / 2) + ')');

            var border_svg = d3.select(this)
                .append('svg')
                .attr('width', Math.ceil(pie_width))
                .attr('height', Math.ceil(pie_height))
                .append('g')
                .attr('transform', 'translate(' + (pie_width / 2) + ',' + (pie_height / 2) + ')');


            var arc = d3.svg.arc()
                .outerRadius(radius);

            var border_arc = d3.svg.arc()
                .innerRadius(radius - pie_border_width)
                .outerRadius(radius);

            var pie = d3.layout.pie()
                .value(function (d) {
                    return d.count;
                })
                .sort(null);

            var path = svg.selectAll('path')
                .data(pie(dataset))
                .enter()
                .append('path')
                .attr('d', arc)
                .attr('fill', function (d, i) {
                    return color(d.data.label);
                });

            var path_border = border_svg.selectAll('path')
                .data(pie(dataset_border))
                .enter()
                .append('path')
                .attr('d', border_arc)
                .attr('fill', function (d, i) {
                    return color(d.data.label);
                });

            $(this).find('svg').css({
                'margin-top': -pie_height / 2,
                'margin-left': -pie_width / 2
            });
        });
    }

    // Tour window toggle button
    var tour_on = false;
    $('.nav-window.tour-window .toggle-button').click(function () {
        var container = $(this).closest('.toggle-container');
        container.toggleClass('open');

        tour_step_window.find('.triangle').each(function () {
            var parent_left = $(this).closest('.window').css('left');
            $(this).css('left', -(parseFloat(parent_left) + 3));
        });

        if (container.hasClass('open')) {
            $('.tour-button.nav-dropdown-button').click();
            $('#tour-container').fadeIn();
            $('#main-header').addClass('passive');
            tour_on = true;
        }
        else {
            reset_tour();
        }
    });

    // Tour window navigation
    var tour_step = $('#tour-container .tour-step');
    var tour_step_count = tour_step.length;
    tour_step.each(function () {
        $(this).prepend('<span class="pulse-dot"></span>');
    });

    var tour_step_window = tour_step.children('.window');
    tour_step_window.each(function () {
        $(this).prepend('<span class="triangle"></span><button class="close-button"><svg width="9" height="10"><use xlink:href="#g-icon-close-black"></use></svg></button>');
        $(this).append('<footer><a class="hide-tour">Hide Tour</a><p><span>' + parseFloat($(this).parent().index() + 1) + '</span> of <span>' + tour_step_count + '</span></p><div class="button-container"><button class="prev"><svg width="7" height="11"><use xlink:href="#g-icon-tour-arrow"></use></svg></button><button class="next"><svg width="7" height="11"><use xlink:href="#g-icon-tour-arrow"></use></svg></button></div></footer>')
    });

    var tour_nav_button = tour_step_window.find('.button-container').children('button');

    tour_nav_button.click(function () {
        var this_tour = $(this).closest('.tour-step');
        var this_tour_index = this_tour.index();

        if ($(this).hasClass('next')) {
            if (this_tour_index + 1 < tour_step_count) {
                $('.tour-step').removeClass('active');
                $('.tour-step').eq(this_tour_index + 1).addClass('active');
            }
            else {
                $('.tour-step').removeClass('active');
                leave_tour();
            }
            if (isMobile && ($('.tour-step.step2').hasClass('active') || $('.tour-step.step3').hasClass('active'))) {
                $('.tour-step.step1').find('.pulse-dot').addClass('active');
            }
            else {
                $('.tour-step.step1').find('.pulse-dot').removeClass('active');
            }
        }
        else {
            if (this_tour_index - 1 >= 0) {
                $('.tour-step').removeClass('active');
                $('.tour-step').eq(this_tour_index - 1).addClass('active');
            }
        }
    });

    tour_step.children('.pulse-dot').click(function () {
        $('.tour-step.active').find('.pulse-dot').addClass('visited');
        $('.tour-step').removeClass('active');
        $(this).closest('.tour-step').addClass('active');
    });

    tour_step.find('.close-button').click(function () {
        leave_tour();
    });

    function reset_tour() {
        $('#tour-container').fadeOut(function () {
            $('#main-header').removeClass('passive');
            $('.tour-step').removeClass('active');
            $('.tour-step:first-child').addClass('active');
            $('.nav-window.tour-window').find('.toggle-container').removeClass('open');
            $('.tour-step.step1').find('.pulse-dot').removeClass('active');
            $('.tour-step').find('.pulse-dot').removeClass('visited');
            tour_on = false;
        });
    }

    function leave_tour() {
        $('.leaving-tour').show();
        $('.tour-button.nav-dropdown-button').click();
        if (isMobile) {
            setTimeout(function () {
                $('html, body').animate({
                    scrollTop: $('.leaving-tour').offset().top
                }, 500);
                setTimeout(function () {
                    reset_tour();
                }, 500);
            }, 300);
        }
        else {
            reset_tour();
        }
        setTimeout(function () {
            setTimeout(function () {
                $('.leaving-tour').slideUp();
            }, 1000);
        }, 3000);

    }

    $('.tour-step').find('.hide-tour').click(leave_tour);
    $('.leaving-tour .close-button').click(function () {
        $('.leaving-tour').slideUp();
    });

    // Video popup
    $('.nav-window.tour-window .video-links ul a').magnificPopup({
        type: 'inline',
        callbacks: {
            open: function () {
                $('.mfp-content video')[0].play();
            }
        },
        closeBtnInside: true
    });

});

$(window).load(function(){
    // Adjust right corner button tooltip positions
    var button_text = $('.pull-right').find('.nav-dropdown-button').children('.button-tooltip');
    button_text.each(function () {
        $(this).show();
        var width_button = $(this).parent().outerWidth();
        var width = $(this).outerWidth();
        $(this).css('left', -((width - width_button) / 2) + 'px').hide();
    });
});

// Create dummy element
$('body').append('<div class="nav-window ticketing-window dummy-container"><div class="dummy"></div></div>');

// Navigating sliding Pages
var ticket_page_list = {
    root: {
        title: 'Root',
        element: ''
    }
    ,
    main: {
        title: 'Tickets',
        element: '#main-header .ticketing-window .ticket-list-container',
        onShow: function ($el) {
            $el.find('header .refresh-button').show();
        },
        onLeave: function ($el) {
            $el.find('header .refresh-button').hide();
        }
    }
    ,
    ticket_detail: {
        title: 'Ticket Detail',
        element: '#main-header .ticketing-window .ticket-content-container',
        parent: 'main',
        onShow: function ($el) {
            $el.find('header .refresh-button').show();
        },
        onLeave: function ($el) {
            $el.find('header .refresh-button').hide();
        }
    }
    ,
    new_ticket: {
        title: 'New Ticket',
        element: '#main-header .ticketing-window .new-ticket-type-selection',
        parent: 'main',
        onShow: function ($el) {
            $el.children('header').find('.new-ticket-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.new-ticket-button').removeClass('active');
        }
    },
    ticket_report_problem: {
        title: 'Report a Problem',
        element: '#main-header .ticketing-window .new-ticket-form.report-problem-form',
        parent: 'new_ticket',
        onShow: function ($el) {
            $el.children('header').find('.new-ticket-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.new-ticket-button').removeClass('active');
        }
    },
    ticket_request_change: {
        title: 'Request a Change',
        element: '#main-header .ticketing-window .new-ticket-form.request-change-form',
        parent: 'new_ticket',
        onShow: function ($el) {
            $el.children('header').find('.new-ticket-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.new-ticket-button').removeClass('active');
        }
    },
    ticket_get_assistance: {
        title: 'Get Assistance',
        element: '#main-header .ticketing-window .new-ticket-form.get-assistance-form',
        parent: 'new_ticket',
        onShow: function ($el) {
            $el.children('header').find('.new-ticket-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.new-ticket-button').removeClass('active');
        }
    },
    ticket_schedule_visit: {
        title: 'Schedule a Visit',
        element: '#main-header .ticketing-window .new-ticket-form.schedule-visit-form',
        parent: 'new_ticket',
        onShow: function ($el) {
            $el.children('header').find('.new-ticket-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.new-ticket-button').removeClass('active');
        }
    },
    search_ticket: {
        title: 'Search',
        element: '#main-header .ticketing-window .search-form',
        parent: 'main',
        onShow: function ($el) {
            $el.children('header').find('.search-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.search-button').removeClass('active');
        }
    },
    search_results: {
        title: 'Search Results',
        element: '#main-header .ticketing-window .search-results',
        parent: 'search_ticket',
        onShow: function ($el) {
            $el.children('header').find('.search-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.search-button').removeClass('active');
        }
    },
    search_no_results: {
        title: 'Search Results',
        element: '#main-header .ticketing-window .search-no-results',
        parent: 'search_ticket',
        onShow: function ($el) {
            $el.children('header').find('.search-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.search-button').removeClass('active');
        }
    }
};
var recovery_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    recovery_links: {
        title: 'Recovery',
        element: '.links-list.recovery-links',
        onShow: function ($el) {
            $el.children('header').find('.recovery-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.recovery-button').removeClass('active');
        }
    }
};
var lacunhboard_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    productivity_links: {
        title: 'Productivity',
        element: '.links-list.productivity-links',
        onShow: function ($el) {
            $el.children('header').find('.productivity-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.productivity-button').removeClass('active');
        }
    },
    administrative_links: {
        title: 'Administrative',
        element: '.links-list.administrative-links',
        onShow: function ($el) {
            $el.children('header').find('.administrative-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.administrative-button').removeClass('active');
        }
    },
    recover_cloud_links: {
        title: 'Recover to Cloud',
        parent: 'productivity_links',
        element: '.links-list.recover-cloud-links',
        onShow: function ($el) {
            $el.children('header').find('button').hide();
        },
        onLeave: function ($el) {
            $el.children('header').find('button').show();
        }
    }
};
var impersonate_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    user_list: {
        title: 'Impersonate',
        element: '.nav-window.impersonate-window .user-list.main',
        onShow: function ($el) {
            impersonate_open = false;
        },
        onLeave: function ($el) {
        }
    },
    user_sub_list: {
        title: 'Impersonate',
        parent: 'user_list',
        element: '.nav-window.impersonate-window .user-list.sub-list',
        onShow: function ($el) {
            var company = $el.parent().find('.selected-company');
            company.find('span').text(chosen_company).attr('data-name', chosen_company);
            setTimeout(function () {
                company.slideDown(300);
            }, 300);
        },
        onLeave: function ($el) {
            setTimeout(function () {
                $el.parent().find('.selected-company').slideUp(300);
            }, 300);
        }
    }
};
var events_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    events_list: {
        title: 'Events',
        element: '.nav-window.events-window .events-list-container',
        onShow: function ($el) {
        },
        onLeave: function ($el) {
        }
    },
    search_events: {
        title: 'Search',
        parent: 'events_list',
        element: '.nav-window.events-window .search-form',
        onShow: function ($el) {
            $el.children('header').find('.search-button').addClass('active');
        },
        onLeave: function ($el) {
            $el.children('header').find('.search-button').removeClass('active');
        }
    },
    search_results: {
        title: 'Search',
        parent: 'search_events',
        element: '.nav-window.events-window .search-results',
        onShow: function ($el) {
        },
        onLeave: function ($el) {
        }
    },
    search_no_results: {
        title: 'Search',
        parent: 'search_events',
        element: '.nav-window.events-window .search-no-results',
        onShow: function ($el) {
        },
        onLeave: function ($el) {
        }
    }
};
var user_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    profile_tab: {
        title: 'Profile',
        element: '.nav-window.user-window .profile-tab',
        onShow: function ($el) {
            $el.find('.profile-tab').children('header').children('h3').hide();
            $el.find('.profile-tab').children('header').children('.user-name').show();
            user_profile.go_to_page('user_info', 'left', true);
            $('.user-window').children('header').find('button.profile-button').addClass('active');
        },
        onLeave: function ($el) {
            $('.user-window').children('header').find('button').removeClass('active');
        }
    },
    contacts_tab: {
        title: 'Contacts',
        element: '.nav-window.user-window .contacts-tab',
        onShow: function ($el) {
            $('.user-window').children('header').find('button.contacts-button').addClass('active');
        },
        onLeave: function ($el) {
            $('.user-window').children('header').find('button').removeClass('active');
        }
    }
};
var user_profile_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    user_info: {
        title: 'Profile',
        element: '.nav-window.user-window .user-info',
        onShow: function ($el) {
        },
        onLeave: function ($el) {
        }
    },
    user_info_edit: {
        title: 'User Info Edit',
        parent: 'user_info',
        element: '.nav-window.user-window .user-info-edit',
        onShow: function ($el) {
            $('.user-edit-button').addClass('active');
            $('.profile-tab').children('header').find('.user-name').hide();
            $('.profile-tab').children('header').find('h3').show();
        },
        onLeave: function ($el) {
            $('.user-edit-button').removeClass('active');
            $('.profile-tab').children('header').find('.user-name').show();
            $('.profile-tab').children('header').find('h3').hide();
        }
    }
};
var mobile_nav_page_list;
var tour_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    info_tab: {
        title: 'Information',
        element: '.nav-window.tour-window .information-tab',
        onShow: function ($el) {
            $('.tour-window').children('header').find('button.information-button').addClass('active');
        },
        onLeave: function ($el) {
            $('.tour-window').children('header').find('button').removeClass('active');
        }
    },
    tutorials_tab: {
        title: 'Tutorials',
        element: '.nav-window.tour-window .tutorials-tab',
        onShow: function ($el) {
            $('.tour-window').children('header').find('button.tutorials-button').addClass('active');
        },
        onLeave: function ($el) {
            $('.tour-window').children('header').find('button').removeClass('active');
        }
    }
};
var r2cwidget_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    overview_tab: {
        title: 'SR Overview',
        element: '.nav-window.widget-window .overview-tab',
        onShow: function ($el) {
            $('.nav-window.widget-window > header').find('button').removeClass('active');
            $('.nav-window.widget-window > header').find('.server-button').addClass('active');
            $('.nav-window.widget-window').find('.summary').slideDown(150);

        },
        onLeave: function ($el) {
            $('.nav-window.widget-window').find('.info').show();
            $('.asset-list-container').slideUp(150, function () {
                $('.nav-window.widget-window').find('.summary').slideDown(150);
                $('.overview-tab').find('.active').removeClass('active');
            });
        }
    },
    volume_tab: {
        title: 'SR Overview',
        element: '.nav-window.widget-window .volume-tab',
        onShow: function ($el) {
            $('.nav-window.widget-window > header').find('button').removeClass('active');
            $('.nav-window.widget-window > header').find('.volume-button').addClass('active');
            $('.nav-window.widget-window').find('.summary').slideDown(150);
        },
        onLeave: function ($el) {
        }
    },
    filter_tab: {
        title: 'Filtering',
        element: '.nav-window.widget-window .filter-tab',
        onShow: function ($el) {
            $('.nav-window.widget-window > header').find('button').removeClass('active');
            $('.nav-window.widget-window > header').find('.filter-button').addClass('active');
            $('.nav-window.widget-window').find('.summary').slideUp(150);
        },
        onLeave: function ($el) {
        }
    },
    report_tab: {
        title: 'Report Generator',
        element: '.nav-window.widget-window .report-tab',
        onShow: function ($el) {
            $('.nav-window.widget-window > header').find('button').removeClass('active');
            $('.nav-window.widget-window > header').find('.report-button').addClass('active');
            $('.nav-window.widget-window').find('.summary').slideUp(150);


            $('.report-tab').find('.wide-header-button').removeClass('active');
            $('.report-tab').find('.quick-reports-button').addClass('active');

            r2cwidget_report.go_to_page('quick_reports_tab', 'left', true);
        },
        onLeave: function ($el) {
        }
    }
};
var r2cwidget_report_page_list = {
    root: {
        title: 'Root',
        element: ''
    },
    quick_reports_tab: {
        title: 'Report Generator',
        element: '.nav-window.widget-window .report-tab .quick-reports',
        onShow: function ($el) {

        },
        onLeave: function ($el) {
        }
    },
    reports_by_date_tab: {
        title: 'Report Generator',
        element: '.nav-window.widget-window .report-tab .reports-by-date',
        onShow: function ($el) {

        },
        onLeave: function ($el) {
        }
    }
};

function MovePages(page_list, $el) {
    var that = this;
    var $nav_window = $el;
    var $tabs = $el.find('.nav-window-content').eq(0).find('>.sliding-tab');
    var current_page = 'root';
    var page_list = page_list;
    var animating = false;
    var $header;
    //init
    if ($nav_window.find('header>h3').length) {
        $header = $nav_window.find('header>h3');
    }
    else {
        $header = $el.closest('.nav-window').find('header>h3');
    }
    //go to page
    this.go_to_page = function (page, direction, force) {
        var offset = $el.find('.nav-window-content').width();
        if (page == current_page) return false;
        if (animating && !force) return false;
        animating = true;
        var _direction = (direction == 'right') ? -1 : 1;
        var $target = $(page_list[page].element);
        var $source = $(page_list[current_page].element);
        //show/hide
        $tabs.hide().css({position: 'absolute'});
        $target.show().css({position: 'relative'});
        $el.find('.nav-window-content').addClass('overflow-hidden');
        $source.show();
        //force
        var anim_speed = force ? 0 : 300;
        //init target position
        $target.transition({x: _direction * offset}, 0);
        //move
        $target.transition({x: 0}, anim_speed, function () {
            animating = false;
            $el.find('.nav-window-content').removeClass('overflow-hidden');
        });
        $source.transition({x: -_direction * offset}, anim_speed, function () {
            if ($source.queue().length <= 1) $source.hide();
            $target.css('transform', 'none').find('.form-buttons').addClass('fixed');
        });
        //refresh header
        that.refresh_header(page);
        //trigger events
        if (page_list[current_page].onLeave) {
            page_list[current_page].onLeave($el);
        }
        if (page_list[page].onShow) {
            page_list[page].onShow($el);
        }
        //set current page
        current_page = page;
    };
    this.redraw_page = function() {
        $(page_list[current_page].element).show();
    }
    this.refresh_header= function(page) {
        if (!page) page=current_page;
        if (page_list[page].parent) {
            $header.find('>span').text(page_list[page_list[page].parent].title);
            $header.addClass('back');
            $header.click(function () {
                that.go_to_page(page_list[page].parent, 'right')
            })
        } else {
            $header.find('>span').text(page_list[page].title);
            $header.removeClass('back');
        }
        
    }
}

var tickets = new MovePages(ticket_page_list, $('#main-header .ticketing-window'));
var recovery = new MovePages(recovery_page_list, $('.portal-window'));
var launchboard = new MovePages(lacunhboard_page_list, $('.tool-window'));
var sungard = new MovePages(impersonate_page_list, $('.impersonate-window .search-results'));
var events = new MovePages(events_page_list, $('.events-window'));
var user = new MovePages(user_page_list, $('.user-window'));
var user_profile = new MovePages(user_profile_page_list, $('.profile-tab'));
var mobile_nav;
var tour = new MovePages(tour_page_list, $('.tour-window'));
var r2cwidget = new MovePages(r2cwidget_page_list, $('.widget-window'));
var r2cwidget_report = new MovePages(r2cwidget_report_page_list, $('.widget-window .report-tab'));
tickets.go_to_page('main');
recovery.go_to_page('recovery_links');
launchboard.go_to_page('productivity_links');
sungard.go_to_page('user_list');
events.go_to_page('events_list');
user.go_to_page('profile_tab');
user_profile.go_to_page('user_info');
tour.go_to_page('info_tab');
r2cwidget_report.go_to_page('quick_reports_tab');
r2cwidget.go_to_page('overview_tab');


// Ticketing window new comment function
function show_new_comment(elem) {
    var tab_button = elem.find('.ticket-tabs').children('.tab-button');
    var tab = elem.find('.ticket-tabs').children('.tab');
    var tab_comments = elem.find('.ticket-tabs .tab-comments');
    var new_comment = tab_comments.find('li:first-child');
    var comment_input = elem.find('.nav-window .comment-input-container textarea');
    tab_button.removeClass('active');
    tab.removeClass('shown');
    elem.find('.tab-button.tab-comments-button').addClass('active');

    tab_comments.addClass('shown');
    comment_input.trigger('focusin').trigger('focusout');

    if (!isMobile) {
        if (tab_comments.scrollTop() == 0) {
            new_comment.hide().css('opacity', '0').addClass('transition new').slideDown();
            transition(400);
        }
        else {
            tab_comments.animate({scrollTop: 0}, 200);
            setTimeout(function () {
                new_comment.hide().css('opacity', '0').addClass('transition new').slideDown();
            }, 400);
            transition(1000);
        }
    }

    var ticket_info_offset = elem.find(".ticket-info").offset().top;
    if (isMobile) {
        if ($('body').scrollTop() == ticket_info_offset) {
            new_comment.hide().css('opacity', '0').addClass('transition new').slideDown();
            transition(400);
        }
        else {
            $('body').animate({scrollTop: ticket_info_offset}, 200);
            setTimeout(function () {
                new_comment.hide().css('opacity', '0').addClass('transition new').slideDown();
            }, 400);
            transition(1000);
        }
    }

    function transition(time) {
        setTimeout(function () {
            new_comment.css('opacity', '1');
            setTimeout(function () {
                new_comment.removeClass('new');
                setTimeout(function () {
                    new_comment.removeClass('transition');
                }, 1000);
            }, 2000);
        }, time);
    }
}

// Ticketing window new ticket function
function show_new_ticket(elem) {
    var ticket_list = elem.find('.ticket-list-container .ticket-list');
    var new_ticket = ticket_list.find('.ticket:first-child');
    new_ticket.hide();
    setTimeout(function () {
        if (ticket_list.scrollTop() == 0) {
            new_ticket.hide().css('opacity', '0').addClass('transition new').slideDown();
            transition(400);
        }
        else {
            ticket_list.animate({scrollTop: 0}, 200);
            setTimeout(function () {
                new_ticket.hide().css('opacity', '0').addClass('transition new').slideDown();
            }, 400);
            transition(1000);
        }
        function transition(time) {
            setTimeout(function () {
                new_ticket.css('opacity', '1');
                setTimeout(function () {
                    new_ticket.removeClass('new');
                    setTimeout(function () {
                        new_ticket.removeClass('transition');
                    }, 1000);
                }, 2000);
            }, time);
        }
    }, 500);
}

// Select CSS changes
function select_color(elem){
    if(elem.children('option:selected').is(':disabled')){
        elem.css('color', '#CCCCCC');
    }
    else{
        elem.css('color', '#4A4A4A');
    }

}