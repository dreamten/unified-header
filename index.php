<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title>Sungard AS</title>

    <!-- FAV ICONS -->
    <link href="assets/header/images/ico/favicon.ico" rel="shortcut icon">
    <link href="assets/header/images/ico/sungard-apple-72x72.png" rel="apple-touch-icon">

    <link rel="shortcut icon" href="">
    <script type="text/javascript" src="assets/header/js/modernizr-custom.js"></script>
    <link href="assets/header/css/reset.css" rel="stylesheet" type="text/css"/>
    <link href="assets/header/css/fonts.css" rel="stylesheet" type="text/css"/>
    <link href="assets/header/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/header/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href="assets/header/css/jquery.tagit.css" rel="stylesheet" type="text/css"/>
    <link href="assets/header/css/magnific-popup.css" rel="stylesheet" type="text/css"/>
    <link href="assets/header/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="assets/header/css/styles-fallback.css" rel="stylesheet" type="text/css"/>
    <link href="assets/header/css/styles-mobile.css" rel="stylesheet" type="text/css"/>

    <link href="assets/header/css/retina.css" rel="stylesheet" type="text/css"/>
    <script src="assets/header/js/fastclick.js" type="text/javascript"></script>
    <!--[if lte IE 8]>
    <script type="text/javascript" src="assets/header/js/selectivizr-min.js"></script>
    <![endif]-->
    <!--[if !(IE 8)]><!-->
    <script src="assets/header/js/d3.min.js" type="text/javascript"></script>
    <!--<![endif]-->
</head>
<body>

<?php include_once("assets/header/images/sprite.svg"); ?>

<main>
    <header id="main-header">

        <span id="logo"></span>

        <!-- The Menu button that appears on mobile -->
        <button class="mobile-toggle nav-dropdown-button">
            <span></span>
        </button>

        <!-- Left Portal Links -->
        <nav class="pull-left">

            <div class="content">
                <!-- Mobile only links -->
                <div class="mobile-links">
                    <header></header>
                    <ul>
                        <li>
                            <a class="recovery-link">Recovery</a>
                        </li>
                        <li>
                            <a class="launchboard-link">Launchboard</a>
                        </li>
                        <li>
                            <a class="sungard-link">Sungard AS</a>
                        </li>
                    </ul>

                </div>

                <!-- Recovery toggle button and window -->
                <div class="button-container portal-window">
                    <button data-name="recovery" class="nav-dropdown-button recovery-button" title="Recovery"
                            tabindex="1">
                        Recovery
                    </button>

                    <div data-name="portal-window" class="nav-window narrow portal-window">
                        <header>
                            <h3 title="Back">
                                <svg width="7" height="11">
                                    <use xlink:href="#g-icon-arrow-left"></use>
                                </svg>
                                <span></span>
                            </h3>
                        </header>
                        <div class="nav-window-content">
                            <ul class="links-list recovery-links has-icons sliding-tab">
                                <li>
                                    <a class="link-managed-services">Managed Services</a>
                                </li>
                                <li>
                                    <a class="link-recovery-service active">Recovery Services</a>
                                </li>
                                <li>
                                    <a class="link-cloud">Self-managed Cloud</a>
                                </li>
                            </ul>


                        </div>
                    </div>
                </div>

                <!-- Launchboard toggle button and window -->
                <div class="button-container tool-window">
                    <button data-name="launchboard" class="nav-dropdown-button tool-button" title="Launchboard"
                            tabindex="2">Launchboard
                    </button>

                    <div data-name="tool-window" class="nav-window narrow tool-window">
                        <header>
                            <h3 title="Back">
                                <svg width="7" height="11">
                                    <use xlink:href="#g-icon-arrow-left"></use>
                                </svg>
                                <span></span>
                            </h3>
                            <button class="productivity-button wide-header-button active">Productivity</button>
                            <button class="administrative-button wide-header-button">Administrative</button>
                        </header>
                        <div class="nav-window-content">
                            <ul class="links-list productivity-links has-icons sliding-tab">
                                <li>
                                    <a class="link-launchboard active">Launchboard</a>
                                </li>
                                <li>
                                    <a class="link-recovery-cloud with-sub">Recover to Cloud (R2C)</a>
                                </li>
                                <li>
                                    <a class="link-test-planning">Test Planning</a>
                                </li>
                                <li>
                                    <a class="link-doc-repo">Document Repository</a>
                                </li>
                                <li>
                                    <a class="link-doc-library">Document Library</a>
                                </li>
                            </ul>
                            <ul class="links-list recover-cloud-links sliding-tab">
                                <li>
                                    <a>Server Replication</a>
                                </li>
                                <li>
                                    <a>Enterprise Storage Replication</a>
                                </li>
                                <li>
                                    <a>Site Replication Manager</a>
                                </li>
                                <li>
                                    <a>iSeries</a>
                                </li>
                            </ul>
                            <ul class="links-list administrative-links has-icons sliding-tab">
                                <li>
                                    <a class="link-customer-management">Customer Management</a>
                                </li>
                                <li>
                                    <a class="link-r2c-admin">R2C Admin</a>
                                </li>
                                <li>
                                    <a class="link-doc-library-repo-admin">Doc Library Repo Admin</a>
                                </li>
                            </ul>


                        </div>
                    </div>
                </div>

                <!-- Impersonate toggle button and window -->
                <div class="button-container impersonate-window">
                    <button data-name="sungard" class="nav-dropdown-button sungard-button" title="Impersonate" tabindex="3">Sungard AS</button>

                    <div data-name="impersonate-window" class="nav-window impersonate-window">
                        <header>
                            <h3 title="Back">
                                <svg width="7" height="11">
                                    <use xlink:href="#g-icon-arrow-left"></use>
                                </svg>
                                <span>Impersonate User</span>
                            </h3>
                        </header>
                        <div class="nav-window-content">
                            <div class="selected-company">
                                <span class="selected-company-span"></span>
                            </div>

                            <div class="input-container">
                                <input type="search" class="input-box" placeholder="Search Users and Companies"/>
                            </div>

                            <div class="search-no-results">
                                <p>
                                    <span>No Results</span>
                                    There are no results for your search criteria.
                                </p>
                            </div>
                            <div class="search-results">
                                <div class="nav-window-content">
                                    <ul class="user-list main sliding-tab">
                                        <li>
                                            <div class="company with-sub">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="company-info">
                                                                <p class="name" data-name="Subaru of America">Subaru of America</p>
                                                                <span>53 Users</span>
                                                                <span>1810 Durham-Chapel Hill Blvd</span>
                                                                <span>Chapel Hill, NC 27514</span>
                                                            </div>
                                                            <div class="user-info">
                                                                <p>Primary Contacts</p>
                                                                <span>Dave Adams</span>
                                                                <span>Chief Technology Officer</span>
                                                                <span>703-555-5555</span>
                                                                <a href="mailto:dadams@subaru.com">dadams@subaru.com</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="company-name" data-name="Subaru of America">Subaru of America</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Anthony Susans">Anthony Susans</p>
                                                                <span>Chief Operations Officer</span>
                                                                <span>703-555-5555</span>
                                                                <a href="mailto:anthony.susans@intralinks.com">anthony.susans@intralinks.com</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Anthony Susans</span>
                                                <span data-company-name="IntraLinks" class="company-name">IntraLinks</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="company with-sub">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="company-info">
                                                                <p class="name" data-name="SunMicrosystems">SunMicrosystems</p>
                                                                <span>X Users</span>
                                                                <span>Address 1</span>
                                                                <span>Address 2</span>
                                                            </div>
                                                            <div class="user-info">
                                                                <p>Primary Contacts</p>
                                                                <span>Contact Name</span>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="company-name" data-name="SunMicrosystems">SunMicrosystems</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="company with-sub">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="company-info">
                                                                <p class="name" data-name="Sutherlands">Sutherlands</p>
                                                                <span>X Users</span>
                                                                <span>Address 1</span>
                                                                <span>Address 2</span>
                                                            </div>
                                                            <div class="user-info">
                                                                <p>Primary Contacts</p>
                                                                <span>Contact Name</span>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="company-name" data-name="Sutherlands">Sutherlands</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Scarlet Smith">Scarlet Smith</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Scarlet Smith</span>
                                                <span data-company-name="Cisco" class="company-name">Cisco</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Bobby Stanley">Bobby Stanley</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Bobby Stanley</span>
                                                <span data-company-name="Siemens" class="company-name">Siemens</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="company with-sub">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="company-info">
                                                                <p class="name" data-name="Subaru of America">Subaru of America</p>
                                                                <span>53 Users</span>
                                                                <span>1810 Durham-Chapel Hill Blvd</span>
                                                                <span>Chapel Hill, NC 27514</span>
                                                            </div>
                                                            <div class="user-info">
                                                                <p>Primary Contacts</p>
                                                                <span>Dave Adams</span>
                                                                <span>Chief Technology Officer</span>
                                                                <span>703-555-5555</span>
                                                                <a href="mailto:dadams@subaru.com">dadams@subaru.com</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="company-name" data-name="Subaru of America">Subaru of America</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Anthony Susans">Anthony Susans</p>
                                                                <span>Chief Operations Officer</span>
                                                                <span>703-555-5555</span>
                                                                <a href="mailto:anthony.susans@intralinks.com">anthony.susans@intralinks.com</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Anthony Susans</span>
                                                <span data-company-name="IntraLinks" class="company-name">IntraLinks</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="company with-sub">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="company-info">
                                                                <p class="name" data-name="SunMicrosystems">SunMicrosystems</p>
                                                                <span>X Users</span>
                                                                <span>Address 1</span>
                                                                <span>Address 2</span>
                                                            </div>
                                                            <div class="user-info">
                                                                <p>Primary Contacts</p>
                                                                <span>Contact Name</span>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="company-name" data-name="SunMicrosystems">SunMicrosystems</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="company with-sub">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="company-info">
                                                                <p class="name" data-name="Sutherlands">Sutherlands</p>
                                                                <span>X Users</span>
                                                                <span>Address 1</span>
                                                                <span>Address 2</span>
                                                            </div>
                                                            <div class="user-info">
                                                                <p>Primary Contacts</p>
                                                                <span>Contact Name</span>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="company-name" data-name="Sutherlands">Sutherlands</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Scarlet Smith">Scarlet Smith</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Scarlet Smith</span>
                                                <span data-company-name="Cisco" class="company-name">Cisco</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Bobby Stanley">Bobby Stanley</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Bobby Stanley</span>
                                                <span data-company-name="Siemens" class="company-name">Siemens</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="company with-sub">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="company-info">
                                                                <p class="name" data-name="Subaru of America">Subaru of America</p>
                                                                <span>53 Users</span>
                                                                <span>1810 Durham-Chapel Hill Blvd</span>
                                                                <span>Chapel Hill, NC 27514</span>
                                                            </div>
                                                            <div class="user-info">
                                                                <p>Primary Contacts</p>
                                                                <span>Dave Adams</span>
                                                                <span>Chief Technology Officer</span>
                                                                <span>703-555-5555</span>
                                                                <a href="mailto:dadams@subaru.com">dadams@subaru.com</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="company-name" data-name="Subaru of America">Subaru of America</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Anthony Susans">Anthony Susans</p>
                                                                <span>Chief Operations Officer</span>
                                                                <span>703-555-5555</span>
                                                                <a href="mailto:anthony.susans@intralinks.com">anthony.susans@intralinks.com</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Anthony Susans</span>
                                                <span data-company-name="IntraLinks" class="company-name">IntraLinks</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="company with-sub">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="company-info">
                                                                <p class="name" data-name="SunMicrosystems">SunMicrosystems</p>
                                                                <span>X Users</span>
                                                                <span>Address 1</span>
                                                                <span>Address 2</span>
                                                            </div>
                                                            <div class="user-info">
                                                                <p>Primary Contacts</p>
                                                                <span>Contact Name</span>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="company-name" data-name="SunMicrosystems">SunMicrosystems</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="company with-sub">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="company-info">
                                                                <p class="name" data-name="Sutherlands">Sutherlands</p>
                                                                <span>X Users</span>
                                                                <span>Address 1</span>
                                                                <span>Address 2</span>
                                                            </div>
                                                            <div class="user-info">
                                                                <p>Primary Contacts</p>
                                                                <span>Contact Name</span>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="company-name" data-name="Sutherlands">Sutherlands</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Scarlet Smith">Scarlet Smith</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Scarlet Smith</span>
                                                <span data-company-name="Cisco" class="company-name">Cisco</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Bobby Stanley">Bobby Stanley</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Bobby Stanley</span>
                                                <span data-company-name="Siemens" class="company-name">Siemens</span>
                                            </div>
                                        </li>

                                    </ul>
                                    <ul class="user-list sub-list sliding-tab">
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Albert Alden">Albert Alden</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Albert Alden</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Sherry Alberts">Sherry Alberts</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Sherry Alberts</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Donald Brenner">Donald Brenner</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Donald Brenner</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Phylis Colt">Phylis Colt</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Phylis Colt</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Ed Edwardson">Ed Edwardson</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Ed Edwardson</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Jack Johnson">Jack Johnson</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Jack Johnson</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Bob Stanley">Bob Stanley</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Bob Stanley</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Albert Alden">Albert Alden</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Albert Alden</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Sherry Alberts">Sherry Alberts</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Sherry Alberts</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Donald Brenner">Donald Brenner</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Donald Brenner</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Phylis Colt">Phylis Colt</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Phylis Colt</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Ed Edwardson">Ed Edwardson</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Ed Edwardson</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Jack Johnson">Jack Johnson</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Jack Johnson</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Bob Stanley">Bob Stanley</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Bob Stanley</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Albert Alden">Albert Alden</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Albert Alden</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Sherry Alberts">Sherry Alberts</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Sherry Alberts</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Donald Brenner">Donald Brenner</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Donald Brenner</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Phylis Colt">Phylis Colt</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Phylis Colt</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Ed Edwardson">Ed Edwardson</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Ed Edwardson</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Jack Johnson">Jack Johnson</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Jack Johnson</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user">
                                                <div class="icon">
                                                    <div class="info">
                                                        <div class="wrapper">
                                                            <div class="user-info">
                                                                <p class="name" data-name="Bob Stanley">Bob Stanley</p>
                                                                <span>Position</span>
                                                                <span>Phone</span>
                                                                <a>Email</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="user-name">Bob Stanley</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


        </nav>

        <!-- Right Portal Links -->
        <nav class="pull-right">
            <div class="buttons-container">
                <div class="button-container">

                    <!-- Ticketing toggle button and window -->
                    <button data-name="tickets" class="tickets-button nav-dropdown-button has-tooltip" title="Ticketing"
                            tabindex="4">
                        <span class="button-tooltip">Ticketing</span>
                        <svg width="18" height="26">
                            <use xlink:href="#g-icon-tickets"></use>
                        </svg>
                    </button>
                    <div class="nav-window ticketing-window">
                        <header>
                            <h3 title="Back">
                                <svg width="7" height="11">
                                    <use xlink:href="#g-icon-arrow-left"></use>
                                </svg>
                                <span>Tickets</span>
                            </h3>
                            <div class="button-container">
                                <button class="refresh-button has-tooltip" title="Refresh">
                                    <span class="button-tooltip">Refresh</span>
                                    <svg width="25" height="20">
                                        <use xlink:href="#g-icon-refresh"></use>
                                    </svg>
                                </button>
                                <button class="search-button has-tooltip" title="Search">
                                    <span class="button-tooltip">Search</span>
                                    <svg width="19" height="20">
                                        <use xlink:href="#g-icon-search"></use>
                                    </svg>
                                </button>
                                <button class="new-ticket-button has-tooltip" title="New Ticket">
                                    <span class="button-tooltip">New Ticket</span>
                                    <svg width="21" height="21">
                                        <use xlink:href="#g-icon-new-ticket"></use>
                                    </svg>
                                </button>
                            </div>
                        </header>
                        <div class="nav-window-content">
                            <div data-title="Tickets" class="ticket-list-container sliding-tab">
                                <div class="refresh-box">
                                    <span title="Refreshing">
                                        <svg width="23" height="18">
                                            <use xlink:href="#g-icon-refresh2"></use>
                                        </svg>
                                    </span>
                                    <span>Refresh</span>
                                </div>
                                <ul class="ticket-list">
                                    <li class="ticket low-pri new">
                                        <a>
                                            <h6>Brown Brothers Harriman has signed a something important</h6>

                                            <p>Scott C. Commented: This server has Mgmt and Backup NIC’s disabled, at
                                                request of customer…</p>
                                            <span>1 hour ago</span>
                                        </a>
                                    </li>
                                    <li class="ticket low-pri">
                                        <a>
                                            <h6>Brown Brothers Harriman has signed a something important</h6>

                                            <p>Scott C. Commented: This server has Mgmt and Backup NIC’s disabled, at
                                                request of customer…</p>
                                            <span>1 hour ago</span>
                                        </a>
                                    </li>
                                    <li class="ticket resolved">
                                        <a>
                                            <h6>Ticket Resolved: Request to login and cycle Image Services</h6>
                                            <span>2 days ago</span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>PA: 2 aurir 5-6 - update vpn40078</h6>

                                            <p>Phlip L. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut
                                                rerum necessitatibus saep…</p>
                                            <span>Aug 28, 2015 11:33 AM</span>
                                        </a>
                                    </li>
                                    <li class="ticket high-pri">
                                        <a>
                                            <h6>GCM Training 8/7</h6>

                                            <p>Scott C. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut
                                                rerum necessitatibus…</p>
                                            <span>Aug 22, 2015 9:56 AM</span>
                                        </a>
                                    </li>
                                    <li class="ticket low-pri">
                                        <a>
                                            <h6>PA: 2 aurir 5-6 - update vpn40078</h6>

                                            <p>Phlip L. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut
                                                rerum necessitatibus saep…</p>
                                            <span>Aug 13, 2015 3:12 PM</span>
                                        </a>
                                    </li>
                                    <li class="ticket high-pri">
                                        <a>
                                            <h6>GCM Training 8/7</h6>

                                            <p>Scott C. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut
                                                rerum necessitatibus…</p>
                                            <span>Aug 22, 2015 9:56 AM</span>
                                        </a>
                                    </li>
                                    <li class="ticket resolved">
                                        <a>
                                            <h6>Ticket Resolved: Request to login and cycle Image Services</h6>
                                            <span>2 days ago</span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>PA: 2 aurir 5-6 - update vpn40078</h6>

                                            <p>Phlip L. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut
                                                rerum necessitatibus saep…</p>
                                            <span>Aug 28, 2015 11:33 AM</span>
                                        </a>
                                    </li>
                                    <li class="ticket high-pri">
                                        <a>
                                            <h6>GCM Training 8/7</h6>

                                            <p>Scott C. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut
                                                rerum necessitatibus…</p>
                                            <span>Aug 22, 2015 9:56 AM</span>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <div class="ticket-content-container sliding-tab">
                                <div class="refresh-box">
                            <span title="Refreshing">
                                <svg width="23" height="18">
                                    <use xlink:href="#g-icon-refresh2"></use>
                                </svg>
                            </span>
                                    <span>Refresh</span>
                                </div>
                                <div class="ticket-content">
                                    <div class="ticket-info">
                                        <h6>PA: 2 aurir 5-6 - update vpn40078</h6>
                                        <ul>
                                            <li>
                                                <span>Ticket #</span>
                                                <span>REQ0270747</span>
                                            </li>
                                            <li>
                                                <span>State</span>
                                                <span>Assessment</span>
                                            </li>
                                            <li>
                                                <span>Last Updated</span>
                                                <span>July 20, 2015 3:15 PM</span>
                                            </li>
                                            <li>
                                                <span>Priority</span>
                                                <span class="pri medium-pri">Medium</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="ticket-tabs">
                                        <button class="tab-button tab-comments-button active">Comments</button>
                                        <button class="tab-button tab-ticket-details-button">Ticket Details</button>
                                        <div class="tab-comments tab shown">
                                            <ul>
                                                <li class="new">
                                                    <p>
                                                        <span>Mohammed Mubeen</span>
                                                        <span>7/21/15 9:15 PM</span>
                                                    </p>

                                                    <p>This server has Mgmt and Backup NIC’s disabled, at request of
                                                        customer. Monitoring tools are operating over the Prod
                                                        Network.</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <span>Mohammed Mubeen</span>
                                                        <span>2/28/15 10:04 PM</span>
                                                    </p>

                                                    <p>Updated Area/Room & Cabinet Information.</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <span>Jared Sheer</span>
                                                        <span>2/15/15 11:09 AM</span>
                                                    </p>

                                                    <p>Lorem ipsum dolor sit amet, consec tetur adipiscing elit.
                                                        Suspendisse
                                                        eu lorem. Lorem ipsum dolor sit amet, consec tetur adipiscing
                                                        elit.</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <span>Mohammed Mubeen</span>
                                                        <span>2/15/15 11:09 AM</span>
                                                    </p>

                                                    <p>This server has Mgmt and Backup NIC’s disabled, at request of
                                                        customer. Monitoring tools are operating over the Prod
                                                        Network.</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <span>Mohammed Mubeen</span>
                                                        <span>2/28/15 10:04 PM</span>
                                                    </p>

                                                    <p>Lorem ipsum dolor sit amet, consec tetur adipiscing elit.
                                                        Suspendisse
                                                        eu lorem. Lorem ipsum dolor sit amet, consec tetur adipiscing
                                                        elit.</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <span>Mohammed Mubeen</span>
                                                        <span>2/28/15 10:04 PM</span>
                                                    </p>

                                                    <p>Updated Area/Room & Cabinet Information.</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <span>Jared Sheer</span>
                                                        <span>2/15/15 11:09 AM</span>
                                                    </p>

                                                    <p>Lorem ipsum dolor sit amet, consec tetur adipiscing elit.
                                                        Suspendisse
                                                        eu lorem. Lorem ipsum dolor sit amet, consec tetur adipiscing
                                                        elit.</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <span>Mohammed Mubeen</span>
                                                        <span>2/15/15 11:09 AM</span>
                                                    </p>

                                                    <p>This server has Mgmt and Backup NIC’s disabled, at request of
                                                        customer. Monitoring tools are operating over the Prod
                                                        Network.</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-ticket-details tab">
                                            <ul>
                                                <li>
                                                    <span>Created</span>
                                                    <span>May 19, 2015 11:00 AM</span>
                                                </li>
                                                <li>
                                                    <span>Company</span>
                                                    <span>Suburu of America</span>
                                                </li>
                                                <li>
                                                    <span>Caller</span>
                                                    <span>Richard Anderson</span>
                                                </li>
                                                <li>
                                                    <span>Impact</span>
                                                    <span>Voluptates Repudiandae</span>
                                                </li>
                                                <li>
                                                    <span>Priority</span>
                                                    <span>Low</span>
                                                </li>
                                                <li>
                                                    <span>Category</span>
                                                    <span></span>
                                                </li>
                                                <li>
                                                    <span>Type</span>
                                                    <span></span>
                                                </li>
                                                <li>
                                                    <span>Item</span>
                                                    <span></span>
                                                </li>
                                                <li>
                                                    <span>Assignment Group</span>
                                                    <span></span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="comment-input-container">
                                        <form>
                                            <textarea placeholder="Add a Comment"></textarea>
                                            <button type="button" class="form-button blue-button post-comment-button">
                                                Post
                                                Comment
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="new-ticket-type-selection sliding-tab">
                                <button class="nt-report-problem" title="Report Problem">
                                    <svg width="36" height="36">
                                        <use xlink:href="#g-icon-report-problem"></use>
                                    </svg>
                                    <span>Report a Problem</span>
                                </button>
                                <button class="nt-request-change" title="Request Change">
                                    <svg width="37" height="37">
                                        <use xlink:href="#g-icon-request-change"></use>
                                    </svg>
                                    <span>Request a Change</span>
                                </button>
                                <button class="nt-get-assistance" title="Get Assistance">
                                    <svg width="37" height="37">
                                        <use xlink:href="#g-icon-get-assistance"></use>
                                    </svg>
                                    <span>Get Assistance</span>
                                </button>
                                <button class="nt-schedule-visit" title="Schedule Visit">
                                    <svg width="37" height="37">
                                        <use xlink:href="#g-icon-schedule-visit"></use>
                                    </svg>
                                    <span>Schedule a Visit</span>
                                </button>

                            </div>
                            <div class="new-ticket-form report-problem-form sliding-tab">
                                <form>
                                    <div class="form">
                                        <p class="ticket-type report-problem">
                                            Report a Problem
                                        </p>

                                        <div class="form-group">
                                            <label for="nt1-input-company">Company *</label>
                                            <select class="input-box" id="nt1-input-company">
                                                <option value="" disabled selected>Select a Company</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt1-input-caller">Caller *</label>
                                            <input type="text" class="input-box" id="nt1-input-caller"
                                                   placeholder="Find a Caller"/>
                                        </div>

                                        <div class="form-group inputs-horizontal">
                                            <label>Impact *</label>

                                            <div>
                                                <div>
                                                    <input type="radio" id="nt1-input-impact-radio1"
                                                           name="nt1-input-impact-radio-group"/>
                                                    <label for="nt1-input-impact-radio1">Something</label>
                                                </div>
                                                <div>
                                                    <input type="radio" id="nt1-input-impact-radio2"
                                                           name="nt1-input-impact-radio-group"/>
                                                    <label for="nt1-input-impact-radio2">Something</label>
                                                </div>
                                                <div>
                                                    <input type="radio" id="nt1-input-impact-radio3"
                                                           name="nt1-input-impact-radio-group"/>
                                                    <label for="nt1-input-impact-radio3">Limited</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group inputs-horizontal">
                                            <label>Priority *</label>

                                            <div>
                                                <div>
                                                    <input type="radio" class="low-pri" id="nt1-input-priority-radio1"
                                                           name="nt1-input-priority-radio-group"/>
                                                    <label for="nt1-input-priority-radio1">Low</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="medium-pri"
                                                           id="nt1-input-priority-radio2"
                                                           name="nt1-input-priority-radio-group"/>
                                                    <label for="nt1-input-priority-radio2">Normal</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="high-pri" id="nt1-input-priority-radio3"
                                                           name="nt1-input-priority-radio-group"/>
                                                    <label for="nt1-input-priority-radio3">High</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="crit-pri" id="nt1-input-priority-radio4"
                                                           name="nt1-input-priority-radio-group"/>
                                                    <label for="nt1-input-priority-radio4">Critical</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt1-input-category">Category *</label>
                                            <select class="input-box" id="nt1-input-category">
                                                <option value="" disabled selected>Select a Category</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt1-input-type">Type *</label>
                                            <select class="input-box" id="nt1-input-type">
                                                <option value="" disabled selected>Select a Type</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt1-input-item">Item *</label>
                                            <select class="input-box" id="nt1-input-item">
                                                <option value="" disabled selected>Select an Item</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt1-input-assignment">Assignment Group *</label>
                                            <input type="search" class="input-box" id="nt1-input-assignment"
                                                   placeholder="Find a Group"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt1-input-summary">Summary *</label>
                                            <textarea class="input-box" id="nt1-input-summary"
                                                      placeholder="Enter Summary"></textarea>
                                        </div>

                                    </div>
                                    <div class="form-buttons double-buttons">
                                        <button type="button"
                                                class="form-button form-button-large grey-button cancel-button">
                                            Cancel
                                        </button>
                                        <button type="button"
                                                class="form-button form-button-large blue-button submit-button">
                                            Create Ticket
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="new-ticket-form request-change-form sliding-tab">
                                <form>
                                    <div class="form">
                                        <p class="ticket-type report-problem">
                                            Request a Change
                                        </p>

                                        <div class="form-group">
                                            <label for="nt2-input-company">Company *</label>
                                            <select class="input-box" id="nt2-input-company">
                                                <option value="" disabled selected>Select a Company</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt2-input-caller">Caller *</label>
                                            <input type="text" class="input-box" id="nt2-input-caller"
                                                   placeholder="Find a Caller"/>
                                        </div>

                                        <div class="form-group inputs-horizontal">
                                            <label>Impact *</label>

                                            <div>
                                                <div>
                                                    <input type="radio" id="nt2-input-impact-radio1"
                                                           name="nt2-input-impact-radio-group"/>
                                                    <label for="nt2-input-impact-radio1">Something</label>
                                                </div>
                                                <div>
                                                    <input type="radio" id="nt2-input-impact-radio2"
                                                           name="nt2-input-impact-radio-group"/>
                                                    <label for="nt2-input-impact-radio2">Something</label>
                                                </div>
                                                <div>
                                                    <input type="radio" id="nt2-input-impact-radio3"
                                                           name="nt2-input-impact-radio-group"/>
                                                    <label for="nt2-input-impact-radio3">Limited</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group inputs-horizontal">
                                            <label>Priority *</label>

                                            <div>
                                                <div>
                                                    <input type="radio" class="low-pri" id="nt2-input-priority-radio1"
                                                           name="nt2-input-priority-radio-group"/>
                                                    <label for="nt2-input-priority-radio1">Low</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="medium-pri"
                                                           id="nt2-input-priority-radio2"
                                                           name="nt2-input-priority-radio-group"/>
                                                    <label for="nt2-input-priority-radio2">Normal</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="high-pri" id="nt2-input-priority-radio3"
                                                           name="nt2-input-priority-radio-group"/>
                                                    <label for="nt2-input-priority-radio3">High</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="crit-pri" id="nt2-input-priority-radio4"
                                                           name="nt2-input-priority-radio-group"/>
                                                    <label for="nt2-input-priority-radio4">Critical</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt2-input-category">Category *</label>
                                            <select class="input-box" id="nt2-input-category">
                                                <option value="" disabled selected>Select a Category</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt2-input-type">Type *</label>
                                            <select class="input-box" id="nt2-input-type">
                                                <option value="" disabled selected>Select a Type</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt2-input-item">Item *</label>
                                            <select class="input-box" id="nt2-input-item">
                                                <option value="" disabled selected>Select an Item</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt2-input-assignment">Assignment Group *</label>
                                            <input type="search" class="input-box" id="nt2-input-assignment"
                                                   placeholder="Find a Group"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt2-input-summary">Summary *</label>
                                            <textarea class="input-box" id="nt2-input-summary"
                                                      placeholder="Enter Summary"></textarea>
                                        </div>

                                    </div>
                                    <div class="form-buttons double-buttons">
                                        <button type="button"
                                                class="form-button form-button-large grey-button cancel-button">
                                            Cancel
                                        </button>
                                        <button type="button"
                                                class="form-button form-button-large blue-button submit-button">
                                            Create Ticket
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="new-ticket-form get-assistance-form sliding-tab">
                                <form>
                                    <div class="form">
                                        <p class="ticket-type report-problem">
                                            Get Assistance
                                        </p>

                                        <div class="form-group">
                                            <label for="nt3-input-company">Company *</label>
                                            <select class="input-box" id="nt3-input-company">
                                                <option value="" disabled selected>Select a Company</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt3-input-caller">Caller *</label>
                                            <input type="text" class="input-box" id="nt3-input-caller"
                                                   placeholder="Find a Caller"/>
                                        </div>

                                        <div class="form-group inputs-horizontal">
                                            <label>Impact *</label>

                                            <div>
                                                <div>
                                                    <input type="radio" id="nt3-input-impact-radio1"
                                                           name="nt3-input-impact-radio-group"/>
                                                    <label for="nt3-input-impact-radio1">Something</label>
                                                </div>
                                                <div>
                                                    <input type="radio" id="nt3-input-impact-radio2"
                                                           name="nt3-input-impact-radio-group"/>
                                                    <label for="nt3-input-impact-radio2">Something</label>
                                                </div>
                                                <div>
                                                    <input type="radio" id="nt3-input-impact-radio3"
                                                           name="nt3-input-impact-radio-group"/>
                                                    <label for="nt3-input-impact-radio3">Limited</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group inputs-horizontal">
                                            <label>Priority *</label>

                                            <div>
                                                <div>
                                                    <input type="radio" class="low-pri" id="nt3-input-priority-radio1"
                                                           name="nt3-input-priority-radio-group"/>
                                                    <label for="nt3-input-priority-radio1">Low</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="medium-pri"
                                                           id="nt3-input-priority-radio2"
                                                           name="nt3-input-priority-radio-group"/>
                                                    <label for="nt3-input-priority-radio2">Normal</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="high-pri" id="nt3-input-priority-radio3"
                                                           name="nt3-input-priority-radio-group"/>
                                                    <label for="nt3-input-priority-radio3">High</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="crit-pri" id="nt3-input-priority-radio4"
                                                           name="nt3-input-priority-radio-group"/>
                                                    <label for="nt3-input-priority-radio4">Critical</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt3-input-category">Category *</label>
                                            <select class="input-box" id="nt3-input-category">
                                                <option value="" disabled selected>Select a Category</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt3-input-type">Type *</label>
                                            <select class="input-box" id="nt3-input-type">
                                                <option value="" disabled selected>Select a Type</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt3-input-item">Item *</label>
                                            <select class="input-box" id="nt3-input-item">
                                                <option value="" disabled selected>Select an Item</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt3-input-assignment">Assignment Group *</label>
                                            <input type="search" class="input-box" id="nt3-input-assignment"
                                                   placeholder="Find a Group"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt3-input-summary">Summary *</label>
                                            <textarea class="input-box" id="nt3-input-summary"
                                                      placeholder="Enter Summary"></textarea>
                                        </div>

                                    </div>
                                    <div class="form-buttons double-buttons">
                                        <button type="button"
                                                class="form-button form-button-large grey-button cancel-button">
                                            Cancel
                                        </button>
                                        <button type="button"
                                                class="form-button form-button-large blue-button submit-button">
                                            Create Ticket
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="new-ticket-form schedule-visit-form sliding-tab">
                                <form>
                                    <div class="form">
                                        <p class="ticket-type report-problem">
                                            Schedule a Visit
                                        </p>

                                        <div class="form-group">
                                            <label for="nt4-input-company">Company *</label>
                                            <select class="input-box" id="nt4-input-company">
                                                <option value="" disabled selected>Select a Company</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt4-input-caller">Caller *</label>
                                            <input type="text" class="input-box" id="nt4-input-caller"
                                                   placeholder="Find a Caller"/>
                                        </div>

                                        <div class="form-group inputs-horizontal">
                                            <label>Impact *</label>

                                            <div>
                                                <div>
                                                    <input type="radio" id="nt4-input-impact-radio1"
                                                           name="nt4-input-impact-radio-group"/>
                                                    <label for="nt4-input-impact-radio1">Something</label>
                                                </div>
                                                <div>
                                                    <input type="radio" id="nt4-input-impact-radio2"
                                                           name="nt4-input-impact-radio-group"/>
                                                    <label for="nt4-input-impact-radio2">Something</label>
                                                </div>
                                                <div>
                                                    <input type="radio" id="nt4-input-impact-radio3"
                                                           name="nt4-input-impact-radio-group"/>
                                                    <label for="nt4-input-impact-radio3">Limited</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group inputs-horizontal">
                                            <label>Priority *</label>

                                            <div>
                                                <div>
                                                    <input type="radio" class="low-pri" id="nt4-input-priority-radio1"
                                                           name="nt4-input-priority-radio-group"/>
                                                    <label for="nt4-input-priority-radio1">Low</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="medium-pri"
                                                           id="nt4-input-priority-radio2"
                                                           name="nt4-input-priority-radio-group"/>
                                                    <label for="nt4-input-priority-radio2">Normal</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="high-pri" id="nt4-input-priority-radio3"
                                                           name="nt4-input-priority-radio-group"/>
                                                    <label for="nt4-input-priority-radio3">High</label>
                                                </div>
                                                <div>
                                                    <input type="radio" class="crit-pri" id="nt4-input-priority-radio4"
                                                           name="nt4-input-priority-radio-group"/>
                                                    <label for="nt4-input-priority-radio4">Critical</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt4-input-category">Category *</label>
                                            <select class="input-box" id="nt4-input-category">
                                                <option value="" disabled selected>Select a Category</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt4-input-type">Type *</label>
                                            <select class="input-box" id="nt4-input-type">
                                                <option value="" disabled selected>Select a Type</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt4-input-item">Item *</label>
                                            <select class="input-box" id="nt4-input-item">
                                                <option value="" disabled selected>Select an Item</option>
                                                <option value="option 0">None</option>
                                                <option value="option 1">option 1</option>
                                                <option value="option 2">option 2</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt4-input-assignment">Assignment Group *</label>
                                            <input type="search" class="input-box" id="nt4-input-assignment"
                                                   placeholder="Find a Group"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="nt4-input-summary">Summary *</label>
                                            <textarea class="input-box" id="nt4-input-summary"
                                                      placeholder="Enter Summary"></textarea>
                                        </div>

                                    </div>
                                    <div class="form-buttons double-buttons">
                                        <button type="button"
                                                class="form-button form-button-large grey-button cancel-button">
                                            Cancel
                                        </button>
                                        <button type="button"
                                                class="form-button form-button-large blue-button submit-button">
                                            Create Ticket
                                        </button>
                                    </div>
                                </form>
                            </div>

                            <div class="search-form sliding-tab">
                                <form>
                                    <div class="form">
                                        <div class="form-group">
                                            <input type="hidden" id="si_summary_hidden_input"
                                                   class="summary_hidden_input">
                                            <label for="si-summary">Summary</label>
                                            <span class="clear-search" title="Clear Search">
                                                Clear Search
                                                    <svg width="9" height="7">
                                                        <use xlink:href="#g-icon-close-icon"></use>
                                                    </svg>
                                            </span>
                                            <ul class="search-tagit input-box"></ul>
                                            <span class="input-note">Separate each search term by commas.</span>
                                        </div>

                                        <div class="form-group">
                                            <label>Created</label>
                                            <input type="text"
                                                   class="datepicker datepicker-from input-box input-box-half"
                                                   id="si-created-from"
                                                   placeholder="From">
                                            <input type="text"
                                                   class="datepicker datepicker-to input-box input-box-half last"
                                                   id="si-created-to"
                                                   placeholder="To">
                                        </div>

                                        <p class="advanced-search">
                                            <span>
                                                <span>
                                                    Advanced Search
                                                </span>
                                                <span>
                                                    <svg width="10" height="4">
                                                        <use xlink:href="#g-icon-arrow-down"></use>
                                                    </svg>
                                                </span>
                                            </span>

                                        </p>

                                        <div class="advanced-search-form">

                                            <div class="form-group">
                                                <label>Last Updated</label>
                                                <input type="text"
                                                       class="datepicker datepicker-from input-box input-box-half"
                                                       id="si-last-updated-from"
                                                       placeholder="From">
                                                <input type="text"
                                                       class="datepicker datepicker-to input-box input-box-half last"
                                                       id="si-last-updated-to"
                                                       placeholder="To">
                                            </div>

                                            <div class="form-group">
                                                <label for="si-ticket-number">Ticket Number</label>
                                                <input type="text" class="input-box" id="si-ticket-number"
                                                       placeholder="Search Ticket Number"/>
                                            </div>

                                            <div class="form-group">
                                                <label for="si-comments">Comments</label>
                                                <input type="text" class="input-box" id="si-comments"
                                                       placeholder="Search Comments"/>
                                            </div>

                                            <div class="form-group">
                                                <label for="si-company">Company</label>
                                                <select class="input-box" id="si-company">
                                                    <option value="" disabled selected>Select a Company</option>
                                                    <option value="option 0">None</option>
                                                    <option value="option 1">option 1</option>
                                                    <option value="option 2">option 2</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="si-caller">Caller</label>
                                                <input type="text" class="input-box" id="si-caller"
                                                       placeholder="Find a Caller"/>
                                            </div>

                                            <div class="form-group inputs-horizontal square">
                                                <label>Impact</label>

                                                <div>
                                                    <div>
                                                        <input type="checkbox" id="si-impact-checkbox1"
                                                               name="si-impact-checkbox-group"/>
                                                        <label for="si-impact-checkbox1">Something</label>
                                                    </div>
                                                    <div>
                                                        <input type="checkbox" id="si-impact-checkbox2"
                                                               name="si-impact-checkbox-group"/>
                                                        <label for="si-impact-checkbox2">Something</label>
                                                    </div>
                                                    <div>
                                                        <input type="checkbox" id="si-impact-checkbox3"
                                                               name="si-impact-checkbox-group"/>
                                                        <label for="si-impact-checkbox3">Limited</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group inputs-horizontal">
                                                <label>Priority</label>

                                                <div>
                                                    <div>
                                                        <input type="radio" class="low-pri" id="si-priority-radio1"
                                                               name="si-priority-radio-group"/>
                                                        <label for="si-priority-radio1">Low</label>
                                                    </div>
                                                    <div>
                                                        <input type="radio" class="medium-pri" id="si-priority-radio2"
                                                               name="si-priority-radio-group"/>
                                                        <label for="si-priority-radio2">Normal</label>
                                                    </div>
                                                    <div>
                                                        <input type="radio" class="high-pri" id="si-priority-radio3"
                                                               name="si-priority-radio-group"/>
                                                        <label for="si-priority-radio3">High</label>
                                                    </div>
                                                    <div>
                                                        <input type="radio" class="crit-pri" id="si-priority-radio4"
                                                               name="si-priority-radio-group"/>
                                                        <label for="si-priority-radio4">Critical</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="si-category">Category</label>
                                                <select class="input-box" id="si-category">
                                                    <option value="" disabled selected>Select a Category</option>
                                                    <option value="option 0">None</option>
                                                    <option value="option 1">option 1</option>
                                                    <option value="option 2">option 2</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="si-type">Type</label>
                                                <select class="input-box" id="si-type">
                                                    <option value="" disabled selected>Select a Type</option>
                                                    <option value="option 0">None</option>
                                                    <option value="option 1">option 1</option>
                                                    <option value="option 2">option 2</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="si-item">Item</label>
                                                <select class="input-box" id="si-item">
                                                    <option value="" disabled selected>Select an Item</option>
                                                    <option value="option 0">None</option>
                                                    <option value="option 1">option 1</option>
                                                    <option value="option 2">option 2</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-buttons double-buttons">
                                        <button type="button"
                                                class="form-button form-button-large grey-button cancel-button">
                                            Cancel
                                        </button>
                                        <button type="button"
                                                class="form-button form-button-large blue-button search-button">
                                            <span>Search</span>
                                            <span class="icon"></span>
                                        </button>
                                    </div>

                                </form>

                            </div>
                            <div class="search-results sliding-tab">
                                <ul class="ticket-list">
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>Brown Brothers Harriman has signed a something important</h6>

                                            <p>Scott C. Commented: This server has Mgmt and Backup NIC’s disabled, at
                                                request of customer…</p>
                                            <span><span class="highlight">10 min ago</span></span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>PA: 2 aurir 5-6 - update vpn40078</h6>

                                            <p>Philip L. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut rerum <span class="highlight">hosting</span> saep…</p>
                                            <span><span class="highlight">9 hr ago</span></span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>GCM Training 8/7</h6>

                                            <p>Scott C. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut rerum necessitatibus…</p>
                                            <span><span class="highlight">2 days ago</span></span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>PA: 2 aurir 5-6 - update vpn40078</h6>

                                            <p>Philip L. Commented: Temporibus Floor 401 quibusdam et aut officiis
                                                debitis</p>
                                            <span><span class="highlight">Aug 19, 2015 11:33 AM</span></span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>Brown Brothers Harriman has signed a renewal…</h6>

                                            <p>Scott C. Commented: This server has Mgmt and Backup NIC’s disabled, at
                                                request of customer…</p>
                                            <span><span class="highlight">Aug 3, 2015 1:26 PM</span></span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>PA: 2 aurir 5-6 - update vpn40078</h6>

                                            <p>Philip L. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut rerum hosting saep…</p>
                                            <span><span class="highlight">Aug 1, 2015 2:14 AM</span></span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>GCM Training 8/7</h6>

                                            <p>Scott C. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut rerum necessitatibus…</p>
                                            <span><span class="highlight">Jul 14, 2015 3:35 PM</span></span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>PA: 2 aurir 5-6 - update vpn40078</h6>

                                            <p>Philip L. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut rerum hosting saep…</p>
                                            <span><span class="highlight">9 hr ago</span></span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>GCM Training 8/7</h6>

                                            <p>Scott C. Commented: Temporibus autem quibusdam et aut officiis debitis
                                                aut rerum necessitatibus…</p>
                                            <span><span class="highlight">2 days ago</span></span>
                                        </a>
                                    </li>
                                    <li class="ticket medium-pri">
                                        <a>
                                            <h6>PA: 2 aurir 5-6 - update vpn40078</h6>

                                            <p>Philip L. Commented: Temporibus Floor 401 quibusdam et aut officiis
                                                debitis</p>
                                            <span><span class="highlight">Aug 19, 2015 11:33 AM</span></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="search-no-results sliding-tab">
                                <p>
                                    <span>No Results</span>
                                    There are no results for your search criteria.
                                </p>

                                <div class="form-buttons">
                                    <button class="form-button form-button-large blue-button search-button">
                                        <span>Try Again</span>
                                        <span class="icon"></span>
                                    </button>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>

                <div class="button-container">

                    <!-- Tour toggle button and window -->
                    <button data-name="tour" class="tour-button nav-dropdown-button has-tooltip" title="Tour"
                            tabindex="5">
                        <span class="button-tooltip">Info</span>
                        <svg width="25" height="25">
                            <use xlink:href="#g-icon-info"></use>
                        </svg>
                    </button>
                    <div class="nav-window tour-window">
                        <header>
                            <button class="information-button wide-header-button active">Information</button>
                            <button class="tutorials-button wide-header-button">Tutorials</button>
                        </header>
                        <div class="nav-window-content">
                            <div class="information-tab sliding-tab">
                                <div class="content">
                                    <div class="servers">
                                        <h4>Servers</h4>

                                        <h5>Chart Colors</h5>

                                        <p>The chart displays colored squares to help identify CPO ranges.</p>

                                        <div class="info-color">
                                            <div class="line healthy">
                                                <div class="server"></div>
                                                <p>
                                                    <span>Healthy</span>
                                                    <span>CPO between 0-20 min</span>
                                                </p>
                                            </div>
                                            <div class="line weak">
                                                <div class="server"></div>
                                                <p>
                                                    <span>Weak</span>
                                                    <span>CPO between 20-30 min</span>
                                                </p>
                                            </div>
                                            <div class="line poor">
                                                <div class="server"></div>
                                                <p>
                                                    <span>Poor</span>
                                                    <span>CPO 30+ min</span>
                                                </p>
                                            </div>
                                            <div class="line exception">
                                                <div class="server"></div>
                                                <p>
                                                    <span>Exception</span>
                                                    <span>CPO Varies (e.g. Initial Sync, Re-Sync, Testing or Unresponsive)</span>
                                                </p>
                                            </div>
                                        </div>

                                        <h5>CPO/Server Tooltip</h5>

                                        <div class="info-tooltip">
                                            <div class="hover-info poor">
                                                <p class="name">prod01east559</p>
                                                <span class="info info-name">Asset Name</span>
                                                <p class="response">20h<span> /30m</span></p>
                                                <span class="info info-response">CPO</span>
                                                <p class="server-status">poor</p>
                                                <span class="info info-volumes">Health</span>
                                                <p class="volumes">28 Volumes</p>
                                            <span class="info info-status">Number of<br/>
                                                Server Volumes</span>
                                                <p class="stat">300MB Out</p>
                                                <span class="info info-stat">Data Transfer<br/> Rate</span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="volumes">
                                        <h4>Volumes</h4>

                                        <h5>Chart Colors</h5>

                                        <p>The chart displays colored circles to help identify RPO Health.</p>

                                        <div class="info-color">
                                            <div class="line healthy">
                                                <div class="volume" data-capacity="100" data-used-space="100"></div>
                                                <p>
                                                    <span>Healthy</span>
                                                    <span>RPO between 0-20 min</span>
                                                </p>
                                            </div>
                                            <div class="line weak">
                                                <div class="volume" data-capacity="100" data-used-space="100"></div>
                                                <p>
                                                    <span>Weak</span>
                                                    <span>RPO between 20-30 min</span>
                                                </p>
                                            </div>
                                            <div class="line poor">
                                                <div class="volume" data-capacity="100" data-used-space="100"></div>
                                                <p>
                                                    <span>Poor</span>
                                                    <span>RPO 30+ min</span>
                                                </p>
                                            </div>
                                            <div class="line exception">
                                                <div class="volume" data-capacity="100" data-used-space="100"></div>
                                                <p>
                                                    <span>Exception</span>
                                                    <span>RPO Varies (e.g. Initial Sync, Re-Sync, Testing or Unresponsive)</span>
                                                </p>
                                            </div>
                                        </div>

                                        <h5>Sizes & Percentages</h5>

                                        <p>Each circle’s size represents volume capacity relative to other volumes. The
                                            pie chart is filled based on current usage versus total capacity.</p>

                                        <div class="info-sizes">
                                            <div class="capacity">
                                                <h6>CAPACITY</h6>
                                                <div class="line">
                                                    <div class="volume smallest" data-capacity="100"
                                                         data-used-space="0"></div>
                                                    <p>
                                                        <span>Smallest</span>
                                                    </p>
                                                </div>
                                                <div class="line">
                                                    <div class="volume largest" data-capacity="100"
                                                         data-used-space="0"></div>
                                                    <p>
                                                        <span>Largest</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="amount">
                                                <h6>AMOUNT USED</h6>
                                                <div class="line">
                                                    <div class="volume vol-0" data-capacity="100" data-used-space="0"></div>
                                                    <p>
                                                        <span>0%</span> Used
                                                    </p>
                                                </div>
                                                <div class="line">
                                                    <div class="volume vol-25" data-capacity="100" data-used-space="25"></div>
                                                    <p>
                                                        <span>25%</span> Used
                                                    </p>
                                                </div>
                                                <div class="line">
                                                    <div class="volume vol-50" data-capacity="100" data-used-space="50"></div>
                                                    <p>
                                                        <span>50%</span> Used
                                                    </p>
                                                </div>
                                                <div class="line">
                                                    <div class="volume vol-75" data-capacity="100" data-used-space="75"></div>
                                                    <p>
                                                        <span>75%</span> Used
                                                    </p>
                                                </div>
                                                <div class="line">
                                                    <div class="volume vol-100" data-capacity="100" data-used-space="100"></div>
                                                    <p>
                                                        <span>100%</span> Used
                                                    </p>
                                                </div>

                                            </div>
                                        </div>

                                        <h5>RPO/Volume Tooltip</h5>

                                        <div class="info-tooltip">
                                            <div class="hover-info poor">
                                                <p class="name">hda1vol2</p>
                                                <span class="info info-name">Asset Name</span>
                                                <p class="response">6h<span> /30m</span></p>
                                                <span class="info info-response">RPO</span>
                                                <p class="server-status">poor</p>
                                                <span class="info info-volumes">Health</span>
                                                <p class="used-space">2GB Used</p>
                                                <span class="info info-used">Total Used</span>
                                                <p class="capacity">50GB Total</p>
                                                <span class="info info-capacity">Capacity</span>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="tutorials-tab sliding-tab">
                                <div class="video-links">
                                    <p>Video Tutorials</p>

                                    <ul>
                                        <li>
                                            <a href="#introduction-video">Introduction to Dashboard</a>

                                            <div id="introduction-video" class="mfp-hide">
                                                <video controls preload="none">
                                                    <source
                                                        src="http://demo.caadas.com/Sungard%20Header/assets/header/videos/sample-video.mp4"
                                                        type="video/mp4"/>
                                                    <object type="application/x-shockwave-flash"
                                                            data="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"
                                                            width="640" height="360">
                                                        <param name="movie"
                                                               value="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"/>
                                                        <param name="allowFullScreen" value="true"/>
                                                        <param name="wmode" value="transparent"/>
                                                        <param name="flashVars"
                                                               value="controls=true&amp;src=http%3A%2F%2Fdemo.caadas.com%2FSungard%2520Header%2Fassets%2header%2videos%2Fsample-video.mp4"/>
                                                    </object>
                                                </video>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="#health-status-video">The Health Status Panel</a>

                                            <div id="health-status-video" class="mfp-hide">
                                                <video controls preload="none">
                                                    <source
                                                        src="http://demo.caadas.com/Sungard%20Header/assets/header/videos/sample-video.mp4"
                                                        type="video/mp4"/>
                                                    <object type="application/x-shockwave-flash"
                                                            data="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"
                                                            width="640" height="360">
                                                        <param name="movie"
                                                               value="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"/>
                                                        <param name="allowFullScreen" value="true"/>
                                                        <param name="wmode" value="transparent"/>
                                                        <param name="flashVars"
                                                               value="controls=true&amp;src=http%3A%2F%2Fdemo.caadas.com%2FSungard%2520Header%2Fassets%2header%2videos%2Fsample-video.mp4"/>
                                                    </object>
                                                </video>

                                            </div>
                                        </li>
                                        <li>
                                            <a href="#view-health-status-video">Viewing and Download Health Status
                                                Panel</a>

                                            <div id="view-health-status-video" class="mfp-hide">
                                                <video controls preload="none">
                                                    <source
                                                        src="http://demo.caadas.com/Sungard%20Header/assets/header/videos/sample-video.mp4"
                                                        type="video/mp4"/>
                                                    <object type="application/x-shockwave-flash"
                                                            data="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"
                                                            width="640" height="360">
                                                        <param name="movie"
                                                               value="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"/>
                                                        <param name="allowFullScreen" value="true"/>
                                                        <param name="wmode" value="transparent"/>
                                                        <param name="flashVars"
                                                               value="controls=true&amp;src=http%3A%2F%2Fdemo.caadas.com%2FSungard%2520Header%2Fassets%2header%2videos%2Fsample-video.mp4"/>
                                                    </object>
                                                </video>

                                            </div>
                                        </li>
                                        <li>
                                            <a href="#rpo-panel-video">The RPO Panel</a>

                                            <div id="rpo-panel-video" class="mfp-hide">
                                                <video controls preload="none">
                                                    <source
                                                        src="http://demo.caadas.com/Sungard%20Header/assets/header/videos/sample-video.mp4"
                                                        type="video/mp4"/>
                                                    <object type="application/x-shockwave-flash"
                                                            data="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"
                                                            width="640" height="360">
                                                        <param name="movie"
                                                               value="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"/>
                                                        <param name="allowFullScreen" value="true"/>
                                                        <param name="wmode" value="transparent"/>
                                                        <param name="flashVars"
                                                               value="controls=true&amp;src=http%3A%2F%2Fdemo.caadas.com%2FSungard%2520Header%2Fassets%2header%2videos%2Fsample-video.mp4"/>
                                                    </object>
                                                </video>

                                            </div>
                                        </li>
                                        <li>
                                            <a href="#storage-panel-video">The Storage Panel</a>

                                            <div id="storage-panel-video" class="mfp-hide">
                                                <video controls preload="none">
                                                    <source
                                                        src="http://demo.caadas.com/Sungard%20Header/assets/header/videos/sample-video.mp4"
                                                        type="video/mp4"/>
                                                    <object type="application/x-shockwave-flash"
                                                            data="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"
                                                            width="640" height="360">
                                                        <param name="movie"
                                                               value="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"/>
                                                        <param name="allowFullScreen" value="true"/>
                                                        <param name="wmode" value="transparent"/>
                                                        <param name="flashVars"
                                                               value="controls=true&amp;src=http%3A%2F%2Fdemo.caadas.com%2FSungard%2520Header%2Fassets%2header%2videos%2Fsample-video.mp4"/>
                                                    </object>
                                                </video>

                                            </div>
                                        </li>
                                        <li>
                                            <a href="#data-transfer-panel-video">The Data Transfer Panel</a>

                                            <div id="data-transfer-panel-video" class="mfp-hide">
                                                <video controls preload="none">
                                                    <source
                                                        src="http://demo.caadas.com/Sungard%20Header/assets/header/videos/sample-video.mp4"
                                                        type="video/mp4"/>
                                                    <object type="application/x-shockwave-flash"
                                                            data="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"
                                                            width="640" height="360">
                                                        <param name="movie"
                                                               value="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"/>
                                                        <param name="allowFullScreen" value="true"/>
                                                        <param name="wmode" value="transparent"/>
                                                        <param name="flashVars"
                                                               value="controls=true&amp;src=http%3A%2F%2Fdemo.caadas.com%2FSungard%2520Header%2Fassets%2header%2videos%2Fsample-video.mp4"/>
                                                    </object>
                                                </video>

                                            </div>
                                        </li>
                                        <li>
                                            <a href="#create-request-video">Creating a Service Request</a>

                                            <div id="create-request-video" class="mfp-hide">
                                                <video controls preload="none">
                                                    <source
                                                        src="http://demo.caadas.com/Sungard%20Header/assets/header/videos/sample-video.mp4"
                                                        type="video/mp4"/>
                                                    <object type="application/x-shockwave-flash"
                                                            data="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"
                                                            width="640" height="360">
                                                        <param name="movie"
                                                               value="http://flashfox.googlecode.com/svn/trunk/flashfox.swf"/>
                                                        <param name="allowFullScreen" value="true"/>
                                                        <param name="wmode" value="transparent"/>
                                                        <param name="flashVars"
                                                               value="controls=true&amp;src=http%3A%2F%2Fdemo.caadas.com%2FSungard%2520Header%2Fassets%2header%2videos%2Fsample-video.mp4"/>
                                                    </object>
                                                </video>

                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="toggle-container">
                            <span>Product Tour</span>

                            <div class="toggle-button">
                                <div class="circle"></div>
                            </div>
                        </div>

                        <div class="leaving-tour">
                            <button class="close-button" title="Close">
                                <svg width="9" height="10">
                                    <use xlink:href="#g-icon-close-black"></use>
                                </svg>
                            </button>
                            <h6>Leaving the Features Tour</h6>

                            <p>Click above to restart</p>
                        </div>
                    </div>
                </div>

                <div class="button-container">

                    <!-- User toggle button and window -->
                    <button data-name="user" class="user-button nav-dropdown-button has-tooltip" title="User"
                            tabindex="6">
                        <span class="button-tooltip">Profile</span>
                        <svg width="25" height="24">
                            <use xlink:href="#g-icon-user"></use>
                        </svg>
                    </button>
                    <div class="nav-window user-window">
                        <header>
                            <button class="profile-button wide-header-button active">Profile</button>
                            <button class="contacts-button wide-header-button">Contacts</button>
                        </header>
                        <div class="nav-window-content">
                            <div class="profile-tab sliding-tab">
                                <p id="password-notification">
                                    An email has been sent to your user account with instructions to change your password.
                                </p>
                                <header>
                                    <h3 title="Back">
                                        <svg width="7" height="11">
                                            <use xlink:href="#g-icon-arrow-left"></use>
                                        </svg>
                                        <span></span>
                                    </h3>
                                    <span class="user-name">Philip Lester</span>

                                    <div class="button-container">
                                        <button class="user-edit-button has-tooltip" title="Edit">
                                            <span class="button-tooltip">Edit</span>
                                            <svg width="21" height="21">
                                                <use xlink:href="#g-icon-edit"></use>
                                            </svg>
                                        </button>
                                        <button class="logout-button has-tooltip" title="Logout">
                                            <span class="button-tooltip">Logout</span>
                                            <svg width="21" height="21">
                                                <use xlink:href="#g-icon-logout"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </header>
                                <div class="nav-window-content">
                                    <div class="user-info sliding-tab">
                                        <ul>
                                            <li>
                                                <span>Company</span>
                                                <span>Suburu of America</span>
                                            </li>
                                            <li>
                                                <span>Title</span>
                                                <span>Director of Aquisitions</span>
                                            </li>
                                            <li>
                                                <span>Phone</span>
                                                <span>(910) 531-0092</span>
                                            </li>
                                            <li>
                                                <span>Email</span>
                                                <span>philip.lester@suburuofamerica.com</span>
                                            </li>
                                            <li>
                                                <span>Password</span>
                                                <a class="change-password">Change Password</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="user-info-edit sliding-tab">

                                        <div class="form-group">
                                            <label for="ui-name"><span>Name *</span></label>
                                            <input type="text" class="input-box" id="ui-name" value="Philip Lester"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="ui-company"><span>Company *</span></label>
                                            <input type="text" class="input-box" id="ui-company" disabled
                                                   value="Subaru of America"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="ui-title"><span>Title *</span></label>
                                            <input type="text" class="input-box" id="ui-title"
                                                   value="Director of Aquisitions"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="ui-phone"><span>Phone *</span></label>
                                            <input type="tel" class="input-box" id="ui-phone" value="(919) 233-4700"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="ui-email"><span>Email *</span></label>
                                            <input type="email" class="input-box" id="ui-email"
                                                   value="philip.lester@subaruofamerica.com"/>
                                        </div>

                                        <div class="form-buttons double-buttons">
                                            <button type="button"
                                                    class="form-button form-button-large grey-button cancel-button">
                                                Cancel
                                            </button>
                                            <button type="button"
                                                    class="form-button form-button-large blue-button save-button">
                                                Save
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="contacts-tab sliding-tab">
                                <div class="input-container">
                                    <input type="search" class="input-box" placeholder="Search Summary"/>
                                </div>

                                <ul class="search-results">
                                    <li>
                                        <p>
                                            George Barbara
                                            <span>Account Manager</span>
                                        </p>
                                        <span>(305) 448-1937</span>
                                        <a>george.barbara@sungardas.com</a>
                                    </li>
                                    <li>
                                        <p>
                                            Chris Booth
                                            <span>MRP SDM</span>
                                        </p>
                                        <span>(305) 448-2082</span>
                                        <a>chris.booth@sungardas.com</a>
                                    </li>
                                    <li>
                                        <p>
                                            Patricia Carrasquill
                                            <span>Resource Analyst</span>
                                        </p>
                                        <span>(305) 448-2483</span>
                                        <a>george.barbara@sungardas.com</a>
                                    </li>
                                    <li>
                                        <p>
                                            George Barbara
                                            <span>Account Manager</span>
                                        </p>
                                        <span>(305) 448-1937</span>
                                        <a>george.barbara@sungardas.com</a>
                                    </li>
                                    <li>
                                        <p>
                                            Chris Booth
                                            <span>MRP SDM</span>
                                        </p>
                                        <span>(305) 448-2082</span>
                                        <a>chris.booth@sungardas.com</a>
                                    </li>
                                    <li>
                                        <p>
                                            Patricia Carrasquill
                                            <span>Resource Analyst</span>
                                        </p>
                                        <span>(305) 448-2483</span>
                                        <a>george.barbara@sungardas.com</a>
                                    </li>
                                    <li>
                                        <p>
                                            George Barbara
                                            <span>Account Manager</span>
                                        </p>
                                        <span>(305) 448-1937</span>
                                        <a>george.barbara@sungardas.com</a>
                                    </li>
                                    <li>
                                        <p>
                                            Chris Booth
                                            <span>MRP SDM</span>
                                        </p>
                                        <span>(305) 448-2082</span>
                                        <a>chris.booth@sungardas.com</a>
                                    </li>
                                    <li>
                                        <p>
                                            Patricia Carrasquill
                                            <span>Resource Analyst</span>
                                        </p>
                                        <span>(305) 448-2483</span>
                                        <a>george.barbara@sungardas.com</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </nav>


        <!-- Tour overlay -->
        <div id="tour-container">
            <div class="overlay"></div>
            <div class="tour-steps-container">

                <!-- Tour Steps -->
                <div class="tour-step step1 active">
                    <div class="window">
                        <h6>Start Here</h6>

                        <p>Use the Recovery panel to corrupti quos dolores et quas molestias excepturi.</p>
                        <ul>
                            <li>
                                <span>1. Soluta Nobisest Eligendi</span>
                            </li>
                            <li>
                                <span>2. Optio Cumque Nihilimpedit Quo Minus</span>
                            </li>
                            <li>
                                <span>3. Idquod Maxime Placeat</span>
                            </li>
                            <li>
                                <span>4. Facere Possimus</span>
                            </li>
                            <li>
                                <span>5. Omnis Voluptas Assumenda Estomnis</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tour-step step2">
                    <div class="window">
                        <h6>Select a Portal</h6>

                        <p>Use this panel to cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod
                            maxime placeat facere possimus, omnis voluptas assumenda es omnis.</p>
                    </div>
                </div>
                <div class="tour-step step3">
                    <div class="window">
                        <h6>Help 3 heading</h6>

                        <p>Help 3 paragraph</p>
                    </div>
                </div>
                <div class="tour-step step4 widget-step">
                    <div class="window">
                        <h6>Help 4 heading</h6>

                        <p>Help 4 paragraph</p>
                    </div>
                </div>
                <div class="tour-step step5">
                    <div class="window">
                        <h6>Help 5 heading</h6>

                        <p>Help 5 paragraph</p>
                    </div>
                </div>
                <div class="tour-step step6">
                    <div class="window">
                        <h6>Help 6 heading</h6>

                        <p>Help 6 paragraph</p>
                    </div>
                </div>
                <div class="tour-step step7">
                    <div class="window">
                        <h6>Help 7 heading</h6>

                        <p>Help 7 paragraph</p>
                    </div>
                </div>

            </div>
        </div>

    </header>

    <footer id="main-footer">
        <p class="copyright">&copy;2015 Sungard Availability Services</p>

        <div class="links">
            <a>Privacy Policy</a>
            <a>Terms of Use</a>
        </div>
    </footer>
</main>


<script src="assets/header/js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="assets/header/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/header/js/tag-it.min.js" type="text/javascript"></script>
<script src="assets/header/js/jquery.transit.min.js" type="text/javascript"></script>
<script src="assets/header/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="assets/header/js/main.js" type="text/javascript"></script>

<!--[if lte IE 9]>
<script src="assets/header/js/placeholders.jquery.min.js" type="text/javascript"></script>
<![endif]-->
</body>
</html>